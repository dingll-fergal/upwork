// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.


var MailBox = {
  container: null,
  isModal: true,
  modalId: null,
  init: function(options){
    this.container = options.container;
    this.modalId = options.modalId;

    if (options["isModal"] != undefined) {
      this.isModal = options.isModal;
    }

    if(this.isModal){
      this.container = $(this.modalId).find('.modal-body');
      this.container.css('height', ($(window).height() - 200));
      this.container.css('overflow-y', 'auto'); 
    }
    

    this.initEvents();

  },
  initEvents: function(){
    if(this.isModal){
      $(document).on('click', '#open_messages', this.openEmailsModel);
      $('#emailModal').on('hidden.bs.modal', function () {
        this.container.html("");
      }); 
    }
    
  },
  openEmailsModel: function(){
    $(MailBox.modalId).modal('show');
  },
  renderContent: function(html_data){
    this.container.html(html_data);
  },
  getNewMessaged: function(conversation_id){
    alert(conversation_id)
    $.ajax({
      type: 'GET',
      url: "/conversations/get_new_messages/",
      data: {id: conversation_id},
      success: function(data){
        alert(conversation_id)
      }
    });
  }

}