class PagesController < ApplicationController
	before_filter :authenticate_user!, only: [:main, :index]
	before_filter :authenticate_admin, only: :admin
	
	def main
		@level1s = Level1.visible.search(params[:query]).paginate(:page => params[:page]).order(params[:order] || "name asc")
		# Needs only level one search as per as clinet requirment
		#@level2s = Level2.visible.search(params[:query]).paginate(:page => params[:page]).order(params[:order] || "date desc")
		#@level3s = Level3.visible.search(params[:query]).paginate(:page => params[:page]).order(params[:order] || "date desc")
		@comments = Comment.all.paginate(:page => params[:page]).order(params[:order] || "created_at desc")
	end

	def index
		@level1s = Level1.visible.order("created_at asc").limit(9)
	end

	def admin
		@setting = Setting.last ? Setting.last : Setting.new
	end

	def about
		
	end

end
