class MailboxController < ApplicationController
  before_action :authenticate_user!

  def inbox
    @messages = mailbox.inbox
    @active = :inbox
    respond_to do |format|
      format.html
      format.js{ render :action => "inbox" }
    end
  end

  def sent
    @messages = mailbox.sentbox
    @active = :sent
    respond_to do |format|
      format.html
      format.js{ render :action => "inbox" }
    end
  end

  def trash
    @messages = mailbox.trash
    @active = :trash
    respond_to do |format|
      format.html
      format.js{ render :action => "inbox" }
    end
  end

end