class SettingsController < ApplicationController
	before_filter :authenticate_admin

	def create
		setting = Setting.new(setting_params)

		if setting.save
			redirect_to admin_path, notice: "Settings have been updated" 
		else
			redirect_to admin_path, notice: "Settings haven't been updated" 
		end
	end

	def update
		setting = Setting.find(params[:id])

		if setting.update(setting_params)
			redirect_to admin_path, notice: "Settings have been updated" 
		else
			redirect_to admin_path, notice: "Settings haven't been updated" 
		end
	end

	private

	def setting_params
    	params.require(:setting).permit(:mode)
  	end 

end
