var TimelineManager = function(){
	var self = this;
	self.currentTimeline = null;
	self.query = null;


	this.config = {
		"level2": {
			"plural": "level2s",
			"child": "level3",
			"parent": "level1",
			"activeElementID": null,
			"timeline": null
		}, 
		"comment": {
			"plural": "comments",
			"child": null,
			"parent": null,
			"activeElementID": null,
			"timeline": null
		}, 
		"level3": {
			"plural": "level3s",
			"child": null,
			"parent": "level2",
			"activeElementID": null,
			"timeline": null
		}, 
		"level1": {
			"plural": "level1s",
			"child": "level2",
			"parent": null,
			"activeElementID": null,
			"timeline": null
		},
		"resource": {
			"plural": "resources",
			"child": null,
			"parent": null,
			"activeElementID": null,
			"timeline": null
		}

	};
	
	initialize();

	// Constractor
	function initialize(){
		window.timelines = self;

		// Retrieve query param
		if (getUrlParameter('query')){
			self.query = getUrlParameter('query');
		};


		// Initialize timelines and save pointers to config
		$(".timeline[data-type]").each(function( index ) {
	  		var type = $(this).data("type");
	  		self.config[type]["timeline"] = new TimelineController(type, self);
		});
	};

	var descriptionViewer = $("#description-viewer");
	var mediaViewer = $("#media-viewer");
	var middleViewer = $("#expansive_middle_view");

	this.renderMiddleView = function (html_view){
		middleViewer.html(html_view);
	};

	// Pluralize word
	this.pluralize = function(word){
		return self.config[word]["plural"]
	};

	// Capitalize word
	this.capitalize = function(word){
		return word.replace(/(?:^|\s)\S/g, function(a) { return a.toUpperCase(); });
	}

	// Add media to viewer
	this.addMediaToViewer = function(media){
		mediaViewer.html(media);
	};

	// Add description to viewer
	this.addDescriptionToViewer = function(description){
		descriptionViewer.html(description);
	};


	// Retrieve the value of parametr from url string
	function getUrlParameter(sParam)
	{
	    var sPageURL = window.location.search.substring(1);
	    var sURLVariables = sPageURL.split('&');
	    for (var i = 0; i < sURLVariables.length; i++) 
	    {
	        var sParameterName = sURLVariables[i].split('=');
	        if (sParameterName[0] == sParam) 
	        {
	            return sParameterName[1];
	        }
	    }
	}      

	// Add elements to timeline and attach listeners
	this.render = function(type, collection){
		$('[data-type=' + type + '] .timeline-collection').first().html(collection);
		timeline(type).reinitialize();
	};

	// Add element to collection of certain type
	this.addElement = function(type, element) {
		timeline(type).addElement(element);
	};

	// Timeline of certain type
	function timeline(type){
		return self.config[type]["timeline"]
	};

}