class Team < ActiveRecord::Base
  belongs_to :competition
  #has_many :matches
  has_many :squads, dependent: :destroy
  has_many :players, through: :squads

  has_many :team_matches, dependent: :destroy
  has_many :matches, through: :team_matches


end
