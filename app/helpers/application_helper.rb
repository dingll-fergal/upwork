module ApplicationHelper

  def timeline_item object
    render partial: "layouts/timeline_item", locals: {object: object}
  end

  def timeline_items collection
    render partial: "layouts/timeline_item", collection: collection, as: :object
  end

  def timeline type, collection
    render partial: "layouts/timeline", locals: {collection: collection, type: type}
  end

  def level1_card card
    render partial: "layouts/l1card", locals: {object: card}
  end

  def load_levels(type, collection)
    render partial: "levels/level", locals: {collection: collection, type: type}
  end

  def load_nested_levels(object)
    collection = params[:query] ? eval("@#{object.class.child_name}s") : []
    if collection
      case object.class.child_name
      when "level2"
        collection = object.level2s.where("level2s.id IN(?)", collection.pluck(:id))
      when "level3"
        collection = collection.where("level2_id = ? ", object.id)
      end 
    end
    collection
    #load_levels(object.class.child_name, collection)
  end

  def load_level_record(collection)
    render partial: "levels/level_items", collection: collection, as: :object
  end

  def item_bg(obj_class_name)
    case obj_class_name
    when "level1"
      "bg-red"
    when "level2"
      "bg-green"
    when "level3"
      "bg-yellow"
    end
  end

  def display_date date, granularity = "day"

    case granularity
      when "minute"
        return date.strftime("%I:%M%p")
      when "day"
        return date.strftime("%B %d, %Y")
      when "month"
        return date.strftime("%B %Y")
      when "year"
        return date.strftime("%Y")
      when "decade"
        return date.strftime("%Y")
    end
  end

  def std_date(date)
   return date if date.blank?
   date.strftime("%d-%B-%Y %H:%M")
  end

  def is_expansive_view?
    params[:viewtype] == 'expansive'
  end
  
  def is_vertical_view?
    params[:viewtype] == 'vertical'
  end
  
  def is_journey_view?
    params[:viewtype] == 'journey'
  end

  def active_page(active_page)
    @active == active_page ? "active" : ""
  end

  def remove_html_tags(html_data="")
    begin
      scrubber = Rails::Html::PermitScrubber.new
      scrubber.tags = ['a']

      html_fragment = Loofah.fragment(html_data)
      html_fragment.scrub!(scrubber)
    rescue Exception => e
      html_fragment = html_data
    end
    html_fragment.to_s
  end


end
