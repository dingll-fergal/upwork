class Comment < ActiveRecord::Base
	# Comment(id: integer, content: string, user_id: integer, commentable_id: integer, commentable_type: string, created_at: datetime, updated_at: datetime)

	belongs_to :commentable, polymorphic: true
	belongs_to :user

	# <return> [Level1, Level2, Level3]
	def object
		commentable_type.constantize.find(commentable_id)
	end

	self.per_page = 30
	attr_accessor :distance_in_time
	validates_presence_of :user, :content, :commentable_type, :commentable_id

	# <overview>  Add :distance_in_time to objects from collection
	# <return> ActiveRecord Collection
	scope :calculate_distance_in_time, lambda{|granularity=nil| 
		time = nil
		all.each do |object|

			this_time = object.created_at
			object.distance_in_time = time ? time_distance(time, this_time, granularity) : 0.01
 
			time = this_time
		end
	}

	# <overview> Distance between two dates
	# <param> [Datetime] First date
	# <param> [Datetime] Second date
	# <param> [String] Granularity
	# <return>	[Integer]
	def self.time_distance(a, b, granularity)
		return 0 unless granularity

		case granularity
		when "minute"
			distance = (a.day - b.day) * 60 * 24 + (a.hour - b.hour) * 60 * (a.minute - b.minute)
		when "day"
			distance = (a.year - b.year) * 365 + (a.month - b.month) * 30 + (a.day - b.day)
		when "month"
			distance = (a.year - b.year) * 12 + (a.month - b.month)
		when "year"
			distance = a.year - b.year
		when "decade"
			distance = (a.year - b.year) / 10
		end
	end

end
