class ApplicationController < ActionController::Base
	protect_from_forgery with: :exception
	helper_method :mailbox

	# reditect to root_path after authentication
	def after_sign_in_path_for(resource)
		root_path
	end

	def filters
		params.require(:filter).permit(:id => [])
	end

	def block_access
		redirect_to root_path, alert: "You do not have sufficient rights to perform this action"
	end

	def authenticate_admin
		block_access unless current_user && (current_user.admin? || current_user.super_user?)
	end

	def fetch_object
		object_class = [Level2, Level1, Level3, Resource].detect{|c| params["#{c.name.underscore}_id"]}
		@object = object_class ? object_class.find(params["#{object_class.name.underscore}_id".to_sym]) : nil
	end

  
  private

  def mailbox
    @mailbox ||= current_user.mailbox
  end

  def conversation
    @conversation ||= mailbox.conversations.find(params[:id])
  end

end
