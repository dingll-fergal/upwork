class CreateLevel1s < ActiveRecord::Migration
  def change
    create_table :level1s do |t|
    	t.string :name, limit: 40
    	t.boolean :visible, default: false
    	t.references :user, null: false, index: true
    	t.datetime :date, index: true, :null => false, :default => Time.now
    	
     	t.attachment :avatar
     	t.text :description
      t.string :slug, uniq: true, index: true


    	t.timestamps null: false
    end
  end
end
