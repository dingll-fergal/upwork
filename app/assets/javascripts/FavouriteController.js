var FavouriteController = function (timeline, userID, manager) {
    var currentPage, timeline, currentElement, userID;

    initialize();

    // Constructor
    function initialize() {
        currentPage = 2;
        adjustTimelineHeight();
        attachListeners();

    };


    function attachListeners() {
        timeline.find(".timeline-content-wrapper").on("click", function () {
            // Activate/Diactivate element on click
            if ($(this).closest(".timeline-block").hasClass("active")) {
                deactivate();
            } else {
                activate($(this).closest(".timeline-block"));
            }
        });

        // Adjust the height of timeline line and load more elements
        timeline.find(".timeline-body").on('scroll', function () {
            adjustTimelineHeight();
            if ($(this).scrollTop() + $(this).innerHeight() >= this.scrollHeight) {
                loadMore();
            }
            ;
        });


        attachCommentListeners();

    };

    function attachCommentListeners() {
        var element = $(".timeline[data-type=comment]");

        // Show input field on click
        element.find(".comment-content").on("click", function () {
            $(this).hide();
            $(this).closest(".comment").find(".comment-input").show();
        });

        // Save changed on click
        element.find(".comment-input [data-action=save]").on("click", function () {
            var element = $(this);
            $.ajax({
                dataType: "json",
                url: element.closest(".comment-input").data("url"),
                method: "patch",
                data: {
                    comment: {
                        content: element.closest(".comment-input").find("input").val()
                    }
                },
                success: function (data) {
                    element.closest(".comment").find(".comment-content").text(data.content);
                    closeCommentForm(element);
                }
            });
        });

        // Hide input on clicking close button
        element.find(".comment-input [data-action=cancel]").on("click", function () {
            closeCommentForm($(this));
        });
    };

    function activate(element) {

        currentElement = element;
        // Add background
        addBackground();

        // Load media and comments
        loadResources();
        //updateHref();


        if (element.closest(".compare-wrapper").length > 0) {

            // Adjust timeline height and margin
            var level3s = element.closest(".compare-wrapper").find(".compare-level3s");

            var distanceInTime = element.closest(".compare-wrapper").find(".distance-in-time");

            if (distanceInTime.height() > 0) {
                var marginTop = distanceInTime.outerHeight(true);
                level3s.css("top", marginTop);
            }


            $(".compare-wrapper .compare-level3s").hide();
            level3s.show();
            level3s.find(".timeline-block").first().addClass("first-child");

            level3s.find(".timeline-line-level3").css("margin-top", level3s.find(".first-child").height() / 2);
            var height = level3s.height() - (level3s.find(".timeline-block").last().height() / 2) - (level3s.find(".timeline-block").first().height() / 2) - $(".roundblock").height()

            level3s.find(".timeline-line-level3").css("height", height);
        }
    };

    function updateHref() {
        if (url = currentURL()) {
            history.replaceState(null, null, url);
        }
    }

    // Find url of current element
    function currentURL() {
        var url = "/level1s/" + currentElement.data("item-name");

        return url;
    };


    // Load media and description
    function loadResources() {
        loadMedia();
        loadDescription();
        loadComments();
    };

    function loadComments() {
        $.ajax({dataType: "script", url: resourceURL("comments")});
    }

    // URL to nested resource
    function resourceURL(resource) {
        if (!currentElement) {
            return "/" + resource;
        }

        return "/" + manager.pluralize(currentElement.data("item-type")) + "/" + currentElement.data("item-id") + "/" + resource;
    }

    // Load media files
    function loadMedia() {
        var url = resourceURL("resources")
        loadResource(url);
    };

    // Load resource
    function loadResource(url, callback) {
        $.ajax({dataType: "script", url: url, success: callback});
    };

    // Load description
    function loadDescription() {
        var url = resourceURL("descriptions")
        loadResource(url);
    };

    // Remove background from all active elements
    function removeBackground() {
        $(".timeline-block").removeClass("active");
    };

    // Add background to active element
    function addBackground() {
        removeBackground();
        currentElement.addClass("active");
    };

    // Deacrivate active element on clicj
    function deactivate() {
        // Remove background
        removeBackground();

        // Hide related andcillaries if any?
        if (currentElement.closest(".compare-wrapper").length > 0) {
            currentElement.closest(".compare-wrapper").find(".compare-level3s").hide();
        }

        currentElement = null;

        // Unload media and description
        loadResources();
    };

    // Load more favourite items
    function loadMore() {
        $.ajax({
            dataType: "script",
            url: "/users/" + userID + "/favourites",
            data: {page: currentPage},
            success: function () {
                currentPage += 1;
            }
        });

    };

    // Adjust timeline height
    function adjustTimelineHeight() {
        var height = timelineHeight();

        if (timeline.find(".compare-wrapper").length > 0) {
            timeline.find(".timeline-line").css("margin-top", (timeline.find(".compare-level2s .timeline-block").first().height() / 2) + timeline.find(".compare-level2s .distance-in-time").height());
        } else {
            timeline.find(".timeline-line").css("margin-top", (timeline.find(".timeline-block").first().height() / 2) + timeline.find(".distance-in-time").height());
        }

        timeline.find(".timeline-line").css("height", height);
    };

    // Calculate timeline height
    function timelineHeight() {
        if (timeline.find(".timeline-wrapper").height() > 0) {


            if (timeline.find(".compare-wrapper").length > 0) {
                console.log(timeline)
                return timeline.find(".timeline-wrapper").height() - (timeline.find(".compare-level2s .timeline-block").last().height() / 2) - (timeline.find(".compare-level2s .timeline-block").first().height() / 2) - 2 * $(".roundblock").height() - $(".distance-in-time").height();
            } else {
                return timeline.find(".timeline-wrapper").height() - (timeline.find(".timeline-block").last().height() / 2) - (timeline.find(".timeline-block").first().height() / 2) - $(".roundblock").height() - $(".distance-in-time").height();
            }

        } else {
            return 0;
        }
        ;
    };

}