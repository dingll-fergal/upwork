class Level3 < ActiveRecord::Base
  # ["id", "name", "visible", "user_id", "level2_id", "date", "avatar_file_name", "avatar_content_type", "avatar_file_size", "avatar_updated_at", "description", "created_at", "updated_at"]
  include Resourceable
  extend FriendlyId
  friendly_id :name, use: [:history, :finders]

  scope :visible, -> { where(visible: true) }

  belongs_to :user
  belongs_to :level2
  has_many :comments, as: :commentable, dependent: :destroy
  has_many :clicks, as: :clickable, dependent: :destroy
  #has_many :resources, as: :mediable, dependent: :destroy
  has_many :descriptions, as: :descriptable, dependent: :destroy
  has_many :favourites, as: :favouritable, dependent: :destroy

  validates_presence_of :name
  validates_length_of :name, :maximum => 40
  validates :level2, :presence => true
  # validates :description, uniqueness: true, :allow_blank => true

  has_attached_file :avatar
  validates_attachment_content_type :avatar, :content_type => [/\Aimage\/.*\Z/]

  self.per_page = 20


  attr_accessor :distance_in_time
  after_save :add_description

  # <overview> All objects whose name contains query
  # <param> [String] query
  # <return>	ActiveRecord Collection
  def self.search query = nil
    result = all
    result = where("LOWER(name) LIKE :q", q: "%#{query.downcase}%") if query
    return result
  end

  # <overview> Helper method for timelines
  # <return> Array
  def parentID
    [level2_id]
  end


  # <overview>  Add :distance_in_time to objects from collection
  # <return> ActiveRecord Collection
  scope :calculate_distance_in_time, lambda { |granularity=nil|
    time = nil
    all.each do |object|

      this_time = object.date
      object.distance_in_time = time ? time_distance(time, this_time, granularity) : 0.01

      time = this_time
    end
  }

  # <overview> Distance between two dates
  # <param> [Datetime] First date
  # <param> [Datetime] Second date
  # <param> [String] Granularity
  # <return>	[Integer]
  def self.time_distance(a, b, granularity)
    return 0 unless granularity

    case granularity
      when "minute"
        distance = (a.day - b.day) * 60 * 24 + (a.hour - b.hour) * 60 * (a.minute - b.minute)
      when "day"
        distance = (a.year - b.year) * 365 + (a.month - b.month) * 30 + (a.day - b.day)
      when "month"
        distance = (a.year - b.year) * 12 + (a.month - b.month)
      when "year"
        distance = a.year - b.year
      when "decade"
        distance = (a.year - b.year) / 10
    end
  end

  # <overview> Update history of description changes
  def add_description
    if descriptions.any?
      if descriptions.last.content != description
        descriptions.create(content: description)
      end
    else
      descriptions.create(content: description)
    end
  end


end
