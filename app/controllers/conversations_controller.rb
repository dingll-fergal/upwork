class ConversationsController < ApplicationController
  before_action :authenticate_user!

  def new
  end

  def create
    recipients = User.where(id: conversation_params[:recipients])
    @conversation = current_user.send_message(recipients, conversation_params[:body], conversation_params[:subject]).conversation
    if request.xhr?
      get_conversations
      render :show
    else
      flash[:success] = "Your message was successfully sent!"
      redirect_to conversation_path(@conversation)
    end

  end

  def update_session
   session[:message_time] =  session[:message_time] = DateTime.now.strftime('%a, %d %b %Y %H:%M:%S')
  end

  def show
    get_conversations
    update_session 
  end

  def get_conversations
    @receipts = conversation.receipts_for(current_user)
    # mark conversation as read
    conversation.mark_as_read(current_user)
  end

  def get_new_messages
    @new_messages = conversation.messages.where("sender_id <> ? AND created_at > ? ", current_user.id, session[:message_time].to_datetime)
    update_session 
  end

  def reply
    current_user.reply_to_conversation(conversation, message_params[:body])
    update_session 
    if request.xhr? 
      get_conversations
      render :show
    else
      flash[:notice] = "Your reply message was successfully sent!"
      redirect_to conversation_path(conversation)
    end
  end

  def destroy
    conversation.move_to_trash(current_user)
    if request.xhr? 
      get_conversations
      render :show
    else
      flash[:success] = 'The conversation was moved to trash.'
      redirect_to conversation_path(conversation)
    end
  end

  def untrash
    conversation.untrash(current_user)
    if request.xhr? 
      get_conversations
      render :show
    else
      flash[:success] = 'The conversation was restored.'
      redirect_to conversation_path(conversation)
    end
  end

  private

  def conversation_params
    params.require(:conversation).permit(:subject, :body,recipients:[])
  end

  def message_params
    params.require(:message).permit(:body, :subject)
  end

end