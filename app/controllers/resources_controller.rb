class ResourcesController < ApplicationController
	before_filter :authenticate_user!
	before_filter :fetch_resource, except: [:index, :create, :new]

	before_filter :authenticate_owner, only: [:update, :destroy]

	before_filter :fetch_object, only: [:index, :new]

	def index

		@resources = @object ? @object.resources : Resource.all
		respond_to do |format|
			format.js
        	format.json { render json: @resources.paths, status: 200 }
    	end
	end

	def new
		@resource = Resource.new
		respond_to do |format|
        	format.html
        	format.js
    	end
	end



	def create
		@resource = Resource.new(resource_params)

		if @resource.save
			respond_to do |format|
	        	format.html { redirect_to @resource.object, notice: 'Resource was successfully created.'}
	        	format.json { render json: @resource, status: 200}
	        	format.js
	    	end
		else
			respond_to do |format|
				format.json { render status: 422}
	        	format.html { redirect_to :back, notice: 'Resource was not created.'}
	        	format.js
	    	end	
		end
	end


	def update	
		if @resource.update(resource_params)
			respond_to do |format|
	        	format.html { redirect_to :back, notice: 'Resource was successfully updated.'}
	        	format.json { render json: @resource, status: 200}
	    	end
		else
			respond_to do |format|
	        	format.html { redirect_to :back, notice: 'Resource was not updated.'}
	        	format.json { render json: @resource.errors, status: 422 }
	    	end			
		end
	end

	def destroy
	    if @resource.destroy      
	    	respond_to do |format|
	        	format.html {redirect_to :back, notice: 'Resource was successfully destroyed.'}
	        	format.json { head 204}
	    	end
	    else
	    	respond_to do |format|
	    		format.html { redirect_to :back, notice: "Resource wasn't destroyed"}
	    		format.json { render json: @resource.errors, status: :unprocessable_entity }
	    	end
	    end
	end

	def show
		if params.has_key?(:clicked_count)
			current_user.clicks.create(clickable_id: @resource.id, clickable_type: @resource.class.to_s)
		end	
		respond_to do |format|
      format.html
      format.js
      format.json { render json: @resource, status: 200, location: @resource}
  	end
	end

	private

	def authenticate_owner
		block_access unless (@resource.object.user == current_user) || current_user.admin? || current_user.super_user?
	end

	def resource_params
    	params.require(:resource).permit(:mediable_id, :mediable_type, :content, :video_url, :professional)
  	end 

  	def fetch_resource
  		@resource = Resource.find(params[:id])
  	end

end
