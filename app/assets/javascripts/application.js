// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require jquery.remotipart
//= require bootstrap.min.js
//= require turbolinks
//= require ekko-lightbox.min.js
//= require ckeditor/init
//= require chosen-jquery
//= require_tree .


// $(document).ready(function(){
// 	$("#header .dropdown").on("click", function(e){
// 		e.preventDefault();
// 		$(this).toggleClass("active");
// 	});
// });

function flashMessage(message){
	if ($(".alert").length  == 0){
		$("body").prepend('<div id="alert-wrapper" class="alert-success"><span><i class="fa fa-check"></i></span><div class="alert">' + message + '</div></div>')
	} else {
		$("#alert-wrapper").fadeIn()
		$("#alert-wrapper .alert").text(message)
	};
	$("#alert-wrapper").delay(7000).fadeOut()
};


$(document).ready(function(){
	if ($("#alert-wrapper").length > 0){
		$("#alert-wrapper").delay(7000).fadeOut()
	}
	$(document).on('ajaxSend', showLoader)
						 .on('ajaxStop', hideLoader);	
})


function showLoader(){
	$('#loader').show();
}

function hideLoader(){
	$('#loader').hide();
}