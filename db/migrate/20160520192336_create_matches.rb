class CreateMatches < ActiveRecord::Migration
  def change
    create_table :matches do |t|
      t.references :group, index: true
      t.references :round, index: true
      t.references :level2, index: true
      t.integer :num
      t.datetime :start_at
      t.datetime :fulltime
      t.string :play_at #loaction
      t.boolean :knockout 
      # t.integer :team1_id
      # t.integer :team2_id
      # t.integer :score1
      # t.integer :score2
      # t.integer :score1et
      # t.integer :score2et
      # t.integer :score1p
      # t.integer :score2p
      # t.integer :team1_score
      # t.integer :team2_score
      # t.integer :winner_team_id


      t.timestamps null: false
    end
  end
end
