class Resource < ActiveRecord::Base
	# Resource(id: integer, content_file_name: string, content_content_type: string, content_file_size: integer, content_updated_at: datetime, mediable_id: integer, mediable_type: string, avatar_file_name: string, avatar_content_type: string, avatar_file_size: integer, avatar_updated_at: datetime, description: text, created_at: datetime, updated_at: datetime) 

	belongs_to :mediable, polymorphic: true
	has_many :comments, as: :commentable, dependent: :destroy
	has_many :descriptions, as: :descriptable, dependent: :destroy

	has_attached_file :content
	validates_attachment :content

	# File content type should include words image or video
	validates_attachment_content_type :content, :content_type => [/\Aimage\/.*\Z/, /\Avideo\/.*\Z/]
	
	# <return> [Level1, Level2, Level3]
	def object
		mediable_type.constantize.find(mediable_id)
	end

	# <overview> Method returns true if file's content type contains word 'image', overwise false
	def is_image_type?
	    content_content_type ? content_content_type =~ %r(image) : false
	end

	# <overview> Method returns true if file's content type contains word 'video', overwise false
	def is_video_type?
	    content_content_type ? content_content_type =~ %r(video) : false
	end

	# <overview> Type of media file
	def resource_type
		if is_image_type?
			return "image" 
		elsif is_video_type?
			return "video"
		else
			return nil
		end
	end

	# def self.paths
	# 	paths = []
	# 	all.each do |resource|
	# 		paths << {src: resource.content.url, type: resource.resource_type}
	# 	end
	# 	return paths
	# end
	
end
