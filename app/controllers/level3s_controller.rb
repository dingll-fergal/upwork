class Level3sController < ApplicationController
  before_filter :authenticate_user!
  before_action :fetch_level2
  before_filter :fetch_level3, only: [:show, :edit, :update, :destroy]
  before_filter :authenticate_owner, only: [:edit, :update, :destroy]

  # GET /level3s.(js|html|json)
  # GET /level2s/:level2_id/level3s.(js|html|json)
  def index
    if @level2
      # Retrieve all visible level2's level3s whose name contain params[:query] ordered by params[:order] or date
      @level3s = @level2.level3s.search(params[:query]).order(params[:order] || "date desc")
      @match = @level2.match
    else
      # Retrieve all elements from @level3s collection whose name contain params[:query] ordered by params[:order] or date
      @level3s = Level3.search(params[:query]).order(params[:order] || "date desc")
    end

    # Retrieve all elements from @level3s collection whose id is included in params[:id]
    if params[:filter]
      @level3s = @level3s.where(filters)
    end

    # Pagination
    if params[:page]
      @level3s = @level3s.visible.paginate(:page => params[:page])
    end

    # Add :distance_in_time field to all elements of @level3s collection
    if params[:granularity]
      @level3s = @level3s.calculate_distance_in_time(params[:granularity])
    end


    respond_to do |format|
      format.html {
        authenticate_admin unless @level2 && @level2.user == current_user
      }
      format.js
      format.json { render json: @level3s, status: 200 }
    end
  end

  # GET /level3s/:id.(html|json)
  def show
    respond_to do |format|
      format.html
      format.js
      format.json { render json: @level3, status: 200, location: @level3 }
    end
  end

  # GET /level2s/:level2_id/level3s/new.(html|js)
  def new
    @level3 = Level3.new
    respond_to do |format|
      format.js
      format.html
    end
  end

  # POST /level2s/:level2_id/level3s.(html|js|json)
  def create
    @level3 = @level2.level3s.new(level3_params)
    @level3.user = current_user

    if @level3.save
      respond_to do |format|
        format.js
        format.html { redirect_to [@level2, @level3], notice: 'Level3 was successfully created.' }
        format.json { render json: @level3, status: 200 }
      end
    else
      respond_to do |format|
        format.js
        format.json { render status: 422 }
        format.html { render :new }
      end
    end
  end

  # GET /level3s/:id/edit.(html|js)
  def edit
    respond_to do |format|
      format.js
      format.html
    end
  end

  # PUT /level3s/:id.(html|js|json)
  def update
    if @level3.update(level3_params)
      respond_to do |format|
        format.js
        format.html { redirect_to [@level2, @level3], notice: 'Level3 was successfully updated.' }
        format.json { render json: @level3, status: 200 }
      end
    else
      respond_to do |format|
        format.js
        format.html { render :edit }
        format.json { render json: @level3.errors, status: 422 }
      end
    end
  end

  # DELETE /level3s/:id.(html|json)
  def destroy
    if @level3.destroy
      respond_to do |format|
        format.html { redirect_to level2_level3s_path(@level2), notice: 'Level3 was successfully destroyed.' }
        format.json { head 204 }
      end
    else
      respond_to do |format|
        format.html { redirect_to :back, notice: "Level3 wasn't destroyed" }
        format.json { render json: @level3.errors, status: :unprocessable_entity }
      end
    end
  end

  private


  def authenticate_owner
    # block access unless user has privilegies or level2's owner
    block_access unless (@level3.level2.user == current_user) || current_user.admin? || current_user.super_user?
  end

  def level3_params
    params.require(:level3).permit(:name, :visible, :level2_id, :date, :avatar, :description, :list_price, :discounted_price)
  end

  def fetch_level2
    @level2 = params[:level2_id] ? Level2.find(params[:level2_id]) : nil
  end

  def fetch_level3
    @level3 = Level3.find(params[:id])

  end

end
