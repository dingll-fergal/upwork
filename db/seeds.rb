# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

images = ["foot1.jpg", "foot2.jpg", "foot3.jpg"]
user = User.first

puts "seed running...."

if !user
  user = User.create(email: "testuser@test.com", password: "12345678")
end

puts "creating levels"
level1 = Level1.where(name: "My League").first
level2s = level1.level2s if level1.present?
level2s.each do |level2|
  level2.destroy
end if level1.present?
level1.destroy if level1.present?


puts "creating season leagues...."
league = League.first
league = League.create(title: "My Football League") unless league.present?

season = Season.first
season = Season.create(title: "First season of this League") unless season.present?


Competition.destroy_all

puts "creating competitions...."
competition = Competition.create(title: "English Premier League", 
                                start_at: DateTime.now,
                                end_at: DateTime.now + 30.days,
                                league: league,
                                season: season) 

["A", "B", "C", "D"].each do |grp_name|
  group = Group.create(competition: competition, title: grp_name)
end

round = Round.create(competition: competition, title: "Round 1", start_at: DateTime.now, end_at: DateTime.now + 20.days)
round = Round.create(competition: competition, title: "Round 2", start_at: DateTime.now + 21.days, end_at: DateTime.now + 30.days)

puts "creating team , squads and players"
60.times.each do |p|
  Player.create(name: Faker::Name.name, num: Faker::Number.between(1, 10))
end unless Player.any?

positions = ["Goalkeeper", "Defender", "Midfielder", "Forword"]

["Man United", "Bournemouth", "Manchester", "Liverpool"].each.with_index(0) do |team_name, index|
  team = Team.create(title: team_name, address: Faker::Address.street_address, web: Faker::Internet.url, competition: competition, num: Faker::Number.between(1, 10))
  players = Player.limit(15).offset(index*15)
  players.each do |player|
    team.squads.create(player: player, pos: positions.sample, num: player.num)
  end
end


level1 = Level1.create(name: "My League", visible: true, user: user, date: DateTime.now, description: "It is a level one")

3.times do |lvl|
  level2 = level1.level2s.create(name: Faker::Team.name, visible: true, user: user, date: DateTime.now, description: Faker::Lorem.paragraphs.first.to_s)  
  level3 = level2.level3s.create(name: Faker::Team.name, visible: true, user: user, date: DateTime.now, description: Faker::Lorem.paragraphs.first.to_s)
  level3 = level2.level3s.create(name: Faker::Team.name, visible: true, user: user, date: DateTime.now, description: Faker::Lorem.paragraphs.first.to_s)
  team1 = Team.all.sample
  team2 = Team.where("id <> ? ", team1.id).sample
  resourse = level2.resources.create(content: File.open(Rails.root.join("db", "images", images.sample)))

  match = Match.create(level2: level2, group: Group.first, round: Round.first, num: lvl, play_at: Faker::Address.city, start_at: DateTime.new(2016, 05, Faker::Number.between(20, 30), 5, 30, 00), fulltime: DateTime.new(2016, 05, 29, 5, 30, 00)  )
  match.team_matches.create(team: team1, score: 4, winner: true)
  match.team_matches.create(team: team2, score: 2, winner: false)
  NewsArticle.create(match: match, level2: level2, title: "news", body: Faker::Lorem.paragraphs(10).join(". "))
end




images.sample