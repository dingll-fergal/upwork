class CreateTeamMatches < ActiveRecord::Migration
  def change
    create_table :team_matches do |t|
      t.references :team, index: true
      t.references :match, index: true
      t.integer :score
      t.boolean :winner

    end
  end
end
