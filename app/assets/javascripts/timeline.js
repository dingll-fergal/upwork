var TimeLine = {
  mainContainer: null,
  levelContainer: null,
  detailContainer: null,
  commentMessageContainer: null,
  currentResourceId: null,
  currentResourceType: null,
  currentSelection: null,
  viewType: null,
  config: null,
  DELAY: 700,
  clicks: 0,
  timer: null,

  init: function(options){ // Intilize function
    this.viewType = options.viewType;

    if(this.viewType != "expansive"){
      return;
    }

    // INtilize container currently only for expansive view
    this.mainContainer = $(options.mainContainer);
    this.levelContainer = $(options.levelContainer);
    this.detailContainer = $(options.detailContainer);
    this.commentMessageContainer = $(options.commentMessageContainer);

    this.defaults();
    this.initEvents();   
  },
  // Check view is expansive or other
  isExpansiveView: function(){
    if(this.viewType == "expansive"){
      return true;
    } else {
      return false;
    }
  },
  // Load default settings
  defaults: function(){
    this.config = {
      "level2": {
        "plural": "level2s",
        "child": "level3",
        "children": "level3s",
        "parent": "level1",
        "activeElementID": null,
        "timeline": null
      }, 
      "comment": {
        "plural": "comments",
        "children": null,
        "child": null,
        "parent": null,
        "activeElementID": null,
        "timeline": null
      }, 
      "level3": {
        "plural": "level3s",
        "child": null,
        "children": null,
        "parent": "level2",
        "activeElementID": null,
        "timeline": null
      }, 
      "level1": {
        "plural": "level1s",
        "child": "level2",
        "children": "level2s",
        "parent": null,
        "activeElementID": null,
        "timeline": null
      },
      "resource": {
        "plural": "resources",
        "children": null,
        "child": null,
        "parent": null,
        "activeElementID": null,
        "timeline": null
      }
    }
  },
  // Initilize all events handlers
  initEvents: function(){
    //this.levelContainer.on('click', '.level', this.singleClickOrDouble);
    this.levelContainer.on('click', '.level', $.proxy(this.loadResources, this));
    this.levelContainer.on('mousedown', '.level', function(e){ e.preventDefault(); });
    this.detailContainer.on('click', '[data-item-type="resource"]', $.proxy(this.loadResourcesComments, this));
    $(document).on('click', '.comment_box_toggel_btn', this.showToggelBox);
  },
  // Toggle commetn button
  showToggelBox: function(){
    $('#comment_box_container').show();
    $(this).hide();
  }, 
  // load commennts for photos and videos
  loadResourcesComments: function(){
    this.currentSelection = $(event.target).closest('[data-item-type="resource"]').first();
    this.updateCurrentResource();
    this.loadComments();
    //this.resourceClicked();
    this.showResource();
  },
  // Show resource on popup
  showResource: function(){
    var self = this;
    $.ajax({ 
      type: 'GET',
      url: "resources/"+ self.currentResourceId,
      data: {clicked_count: true},
      dataType: "script"
    });
  },

  resourceClicked: function(){
    var self = this;
    $.ajax({ 
      dataType: "json",
      method: "post", 
      url:  "/clicks",
      data: {
        "click": {
          "clickable_type": self.capitalize(self.currentResourceType),
          "clickable_id": self.currentResourceId
        }
      }
    });
  },  
  // capitalize current resource used for polymorphic assosications
  capitalize: function(word){
    return word.replace(/(?:^|\s)\S/g, function(a) { return a.toUpperCase(); });
  },
  // load levels and middle view
  loadResources: function(event){
    var self = this;
    this.currentSelection = $(event.target).closest('.level');
    self.updateCurrentResource();
    // if(self.currentSelection.hasClass('selectedItem')){
    //   self.clearResourceOption();
    // } else {
      self.currentSelection.addClass('selectedItem');
      self.loadChildResources();
      if (self.currentSelection.hasClass('floating_levels')) {
        self.removeNextFloatingDiv();
      } else {
        self.removeLevelsAndAddtoFloatingDiv();
      }
      
      self.loadDetailSection();
      //self.removeSimilarLevels();
      //this.loadComments();
    //}

    //self.clicks++;  //count clicks
    // if(self.clicks === 1) {
    //     self.timer = setTimeout(function() {
    //       self.clicks = 0;  //after action performed, reset counter
    //       self.loadDetailSection();
    //     }, self.DELAY);

    // } else {
    //   clearTimeout(self.timer);  //prevent single-click action
    //   self.clicks = 0;  //after action performed, reset counter
    //   if(self.currentSelection.hasClass('selectedItem')){
    //     self.clearResourceOption();
    //   } else {
    //     self.currentSelection.addClass('selectedItem');
    //     self.removeSimilarLevels();
    //     self.loadChildResources();
    //     //this.loadComments();
    //   }
    // }
    event.preventDefault();
  },
  // load middle section
  loadDetailSection: function(){
    var url = this.config[this.currentResourceType]["plural"] + "/" + this.currentResourceId
      if (url){
        $.ajax({
          type: 'GET',
          url: url,
          dataType: "script"
        }); 
      }
  },
  removeNextFloatingDiv: function(){
    this.currentSelection.nextAll().each(function(){
      $(this).fadeOut( "fast", function() {
        $(this).remove();
      });
    });
  },
  removeLevelsAndAddtoFloatingDiv: function(){
    $('#left_view_levels').hide();
    var levelDiv = document.createElement('div');
    // // var currentResource = $('[data-item-type="'+this.currentResourceType+'"][data-item-id="'+this.currentResourceId+'"]').first();
    var bgClass = this.getLevelClass();

    $(levelDiv).addClass('floating_levels color-white level').addClass(bgClass);

    this.currentSelection.each(function() {
      $.each(this.attributes, function() {
        // this.attributes is not a plain object, but an array
        // of attribute nodes, which contain both the name and value
        if(this.specified && this.name != "id" && this.name != "class") {
          $(levelDiv).attr(this.name, this.value);
        } 
      });
    });

    //$(levelDiv).text(this.currentSelection.attr('data-item-name'));

    $(levelDiv).html(this.currentSelection.find('.card-content'));
    if (this.isExpansiveView){
      $('.floating_container').append($(levelDiv));
    }
    TimeLine.renderChildLevels("");
  },

  getLevelClass: function(){
    var bgClass = "";
    if (this.currentResourceType == 'level1'){
      bgClass = "bg-red";
    } 
    else if (this.currentResourceType == 'level2'){
      bgClass = "bg-green";
    } 
    else if (this.currentResourceType == 'level3'){
      bgClass = "bg-yellow";
    }
    return bgClass;
  },


  //remove similar levels when click on any level
  removeSimilarLevels: function(){
    if(this.currentResourceType == "level1"){
      this.currentSelection.parent().prevAll('[data-type-container="level1"]').each(function(){
        $(this).fadeOut( "fast", function() {
          $(this).remove();
        });
      });
      this.currentSelection.parent().nextAll('[data-type-container="level1"]').each(function(){
        $(this).fadeOut( "fast", function() {
          $(this).remove();
        });
      });
    } else if(this.currentResourceType == "level2"){
      this.currentSelection.nextAll().each(function(){
        $(this).fadeOut( "fast", function() {
          $(this).remove();
        });
      });
      this.currentSelection.prevAll().each(function(){
        $(this).fadeOut( "fast", function() {
          $(this).remove();
        });
      });
    }
  },
  // check currentr selection is level one or two
  levelOneOrTwo: function(){
    if(this.currentResourceType == "level1" || this.currentResourceType == "level2"){
      return true;
    } else {
      return false;
    }
  },

  clearResourceOption: function(){ // reset data if click again on the same item
    var self = this;
    this.currentSelection.removeClass('selectedItem');
    // if(this.currentResourceType != "level3"){
      
    // }
    this.detailContainer.html('');
    this.currentResourceId = null;
    this.currentResourceType = null;
    this.commentMessageContainer.html('');
    //this.currentSelection.next().html('');
    if (!self.currentSelection.hasClass('floating_levels')) {
      this.showHideContent(this.currentSelection.next(), "", false);
    }
  },
  // show hide data in passed container
  showHideContent: function(container, htmlData, show){
    if(show){
      container.fadeIn( "fast", function() {
        container.html(htmlData);
      });
    } else {
      container.fadeOut( "fast", function() {
        container.html(htmlData);
      });
    }
  },
  // store last clicked item details in js
  updateCurrentResource: function(){
    this.currentResourceId = this.currentSelection.data('item-id');
    this.currentResourceType = this.currentSelection.data('item-type');
    console.log(this.currentSelection.data('item-id'));
  },
  reloadComments: function(){
    if(this.currentResourceId){
      this.loadComments();
    }
  },
  // load comments
  loadComments: function(){
    var url = this.config[this.currentResourceType]["plural"] + "/" + this.currentResourceId + "/" + "comments";
    if (url){
      this.commentMessageContainer.show();
      $.ajax({
        type: 'GET',
        url: url,
        data: {new_view: true, load_form: true},
        dataType: "script"
      }); 
    }
  },
  // load child resources
  loadChildResources: function(){
    if(this.currentResourceType == "level1" || this.currentResourceType == "level2"){
      var url = this.config[this.currentResourceType]["plural"] + "/" + this.currentResourceId + "/" + this.config[this.currentResourceType]["children"];
      if (url){
        $.ajax({
          type: 'GET',
          url: url,
          data: {new_view: true},
          dataType: "script"
        }); 
      }
      
    }
  },
  renderChildLevels: function(htmlData){
    if (this.isExpansiveView){
      if(htmlData == ""){
        $('#left_view_levels').html(htmlData);
      } else {
        $('#left_view_levels').fadeIn( "fast", function() {
          $('#left_view_levels').html(htmlData);
        });
      }
    }
  },
  // render child resources in containers if available else create
  // deprecated as new interface only need child in main container
  renderInChildContainer: function(type, id, htmlData){
    var child = this.config[type]["child"];
    var childContainer = $('[data-item-type="'+type+'"][data-item-id="'+id+'"]').first().next();
    // var childContainer = $('[data-item-type="'+type+'"][data-item-id="'+id+'"]').parent().find('[data-type-container="' +child +'"]'); 

    if(childContainer && childContainer.attr('data-type-container') == child){
      childContainer.html(htmlData);
    } else {
      var d = document.createElement('div');
      // var childContainer = $(d).attr("data-type-container", child).appendTo($('[data-item-type="'+type+'"][data-item-id="'+id+'"]').parent())
      $('[data-item-type="'+type+'"][data-item-id="'+id+'"]').after($(d).attr("data-type-container", child));
      var childContainer = $('[data-item-type="'+type+'"][data-item-id="'+id+'"]').parent().find('[data-type-container="' +child +'"]');
      childContainer.html(htmlData);
      if (type == "level2" && !childContainer.hasClass('mCustomScrollbar')){
        childContainer.addClass("cards-container scroll mCustomScrollbar search-list _mCS_1");
        childContainer.attr("data-mcs-theme", "dark");
        this.reintilizeScroll();
      }
    }
    this.showHideContent(childContainer, htmlData, true);
    
  },
  renderMiddleView: function (html_view){
    this.detailContainer.html(html_view);
  },
  reintilizeScroll: function(){
    $(".cards-container").mCustomScrollbar({
      theme:"dark",
      autoHideScrollbar: false,
      scrollbarPosition:"inside",
      autoExpandScrollbar:true,
    });
  },
  loadSearchStringRecords: function(htmlData){
    if(this.viewType == "expansive"){
      this.levelContainer.html(htmlData);
      this.clearResourceOption();
    }
  },

  singleClickOrDouble: function(event){
    var self = this;
    var clicks = self.clicks;
    self.clicks++;  //count clicks
    if(self.clicks === 1) {
        self.timer = setTimeout(function() {
            clicks = self.clicks;
            self.clicks = 0;  //after action performed, reset counter
            alert("single")
        }, self.DELAY);

    } else {

        clearTimeout(self.timer);  //prevent single-click action
        clicks = self.clicks;
        self.clicks = 0;  //after action performed, reset counter
        alert("double")
    }
    event.preventDefault();
  }



};