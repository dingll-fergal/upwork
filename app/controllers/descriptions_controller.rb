class DescriptionsController < ApplicationController
	before_filter :authenticate_user!
	before_filter :fetch_description, except: [:index, :create]

	before_filter :authenticate_owner, only: [:update, :destroy]
	before_filter :fetch_object, only: :index


	def index
		@descriptions = @object ? @object.descriptions : Description.all
		respond_to do |format|
			format.js
        	format.json { render json: @descriptions, status: 200 }
    	end
	end



	def create
		@description = Description.new(description_params)

		if @description.save
			respond_to do |format|
	        	format.html { redirect_to :back, notice: 'Description was successfully created.'}
	        	format.json { render json: @description, status: 200}
	    	end
		else
			respond_to do |format|
				format.json { render status: 422}
	        	format.html { redirect_to :back, notice: 'Description was not created.'}
	    	end	
		end
	end


	def update	
		if @description.update(description_params)
			respond_to do |format|
	        	format.html { redirect_to :back, notice: 'Description was successfully updated.'}
	        	format.json { render json: @description, status: 200}
	    	end
		else
			respond_to do |format|
	        	format.html { redirect_to :back, notice: 'Description was not updated.'}
	        	format.json { render json: @description.errors, status: 422 }
	    	end			
		end
	end

	def destroy
	    if @description.destroy      
	    	respond_to do |format|
	        	format.html {redirect_to :back, notice: 'Description was successfully destroyed.'}
	        	format.json { head 204}
	    	end
	    else
	    	respond_to do |format|
	    		format.html { redirect_to :back, notice: "Description wasn't destroyed"}
	    		format.json { render json: @description.errors, status: :unprocessable_entity }
	    	end
	    end
	end

	private

	def authenticate_owner
		block_access unless (@description.object.user == current_user) || current_user.admin? || current_user.super_user?
	end

	def description_params
    	params.require(:description).permit(:descriptable_id, :descriptable_type, :content)
  	end 

  	def fetch_description
  		@description = Description.find(params[:id])
  	end

end
