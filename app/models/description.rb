class Description < ActiveRecord::Base
	# Description(id: integer, content: text, descriptable_id: integer, descriptable_type: string, created_at: datetime, updated_at: datetime)

	belongs_to :descriptable, polymorphic: true
	validates_presence_of :content

	# <return> [Level1, Level2, Level3]
	def object
		descriptable_type.constantize.find(descriptable_id)
	end

	
end
