class AddProfessionalToResources < ActiveRecord::Migration
  def change
    add_column :resources, :professional, :boolean, default: false
  end
end
