class UsersController < ApplicationController
	before_filter :authenticate_user!
	before_filter :fetch_user, only: [:show, :edit, :update, :destroy]
	before_filter :authenticate_owner, except: :index


	def index
		@users = User.all
		respond_to do |format|
        	format.html
        	format.json { render json: @users, status: 200 }
    	end
	end

	def show
		respond_to do |format|
        	format.html
        	format.json { render json: @user, status: 200, location: @user}
    	end
	end


	def edit
	end

	def update	

		if @user.update_attributes(user_params)
			respond_to do |format|
	        	format.html { redirect_to @user, notice: 'User was successfully updated.'}
	        	format.js {render json: @user, status: 200}
	        	format.json { render json: @user, status: 200}
	    	end
		else
			respond_to do |format|
	        	format.html { render :edit}
	        	format.js {render nothing: true, status: 500}
	        	format.json { render json: @user.errors, status: 422 }
	    	end			
		end
	end

	def destroy
	    if @user.destroy      
	    	respond_to do |format|
	        	format.html {redirect_to @user, notice: 'User was successfully destroyed.'}
	        	format.json { head 204}
	    	end
	    else
	    	respond_to do |format|
	    		format.html { redirect_to :back, notice: "User wasn't destroyed"}
	    		format.json { render json: @user.errors, status: :unprocessable_entity }
	    	end
	    end
	end

	private

	def authenticate_owner
		block_access unless (@user == current_user) || current_user.admin? || current_user.super_user?
	end

	def user_params
    	params.require(:user).permit(:id, :first_name, :last_name, :email, :enabled, :role, :avatar, :authenticated)
  	end 

  	def fetch_user
  		@user = User.find(params[:id])
  	end

end
