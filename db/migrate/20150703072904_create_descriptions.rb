class CreateDescriptions < ActiveRecord::Migration
  def change
    create_table :descriptions do |t|
    	t.text :content
    	t.references :descriptable, polymorphic: true, index: true
    
    	t.timestamps null: false
    end
  end
end
