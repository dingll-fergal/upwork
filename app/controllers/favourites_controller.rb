class FavouritesController < ApplicationController
	before_filter :authenticate_user!
	before_filter :fetch_favourite, except: [:create, :index]
	before_filter :fetch_user, only: [:index]


	before_filter :authenticate_owner, only: [:index]

	def index
		@favourites = @user.favourites.paginate(:page => params[:page])
	end

	def create
		@favourite = current_user.favourites.new(favourite_params)

		if @favourite.save
			respond_to do |format|
				format.js
	        	format.html { redirect_to :back, notice: (@favourite.object.class.to_s + " was added to favourite.")}
	        	format.json { render json: @favourite, status: 200}
	    	end
		else
			respond_to do |format|
				format.json { render status: 422}
	        	format.html { render :new}
	    	end	
		end
	end


	def destroy
		@object = @favourite.object

	    if @favourite.destroy      
	    	respond_to do |format|
	    		format.js
	        	format.html {redirect_to :back, notice: (@object.class.to_s + " was removed from favourites.")}
	        	format.json { head 204}
	    	end
	    else
	    	respond_to do |format|
	    		format.html { redirect_to :back, notice: "Favourite wasn't destroyed"}
	    		format.json { render json: @favourite.errors, status: :unprocessable_entity }
	    	end
	    end
	end

	private

	def authenticate_owner
		block_access unless (@user == current_user) || current_user.admin? 
	end

	def fetch_user
		@user = User.find(params[:user_id])
	end

	def favourite_params
    	params.require(:favourite).permit(:favouritable_id, :favouritable_type)
  	end 

  	def fetch_favourite
  		@favourite = Favourite.find(params[:id])
  		block_access unless (@favourite.user == current_user) || current_user.admin? || current_user.super_user?
  	end

end
