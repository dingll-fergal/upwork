class Favourite < ActiveRecord::Base
	# Favourite(id: integer, user_id: integer, favouritable_id: integer, favouritable_type: string, created_at: datetime, updated_at: datetime)

	belongs_to :user
	belongs_to :favouritable, polymorphic: true

	validates_presence_of :user, :favouritable_type, :favouritable_id
	validates_uniqueness_of :user_id, :scope => [:favouritable_type, :favouritable_id]

	self.per_page = 20

	# <return> [Level1, Level2, Level3]
	def object
		favouritable_type.constantize.find(favouritable_id)
	end

	

	
	
end
