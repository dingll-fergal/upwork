class CreateGroups < ActiveRecord::Migration
  def change
    create_table :groups do |t|
      t.references :competition, index: true
      t.string :title
      t.integer :num

      t.timestamps null: false
    end
  end
end
