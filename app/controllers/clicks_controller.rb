class ClicksController < ApplicationController

	before_filter :authenticate_user!
	before_filter :fetch_user, only: :index
	before_filter :fetch_click, only: :destroy


	def index
		@clicks = @user.clicks.all
		respond_to do |format|
        	format.html
        	format.json { render json: @clicks, status: 200 }
    	end
	end

	def create
		@click = current_user.clicks.new(click_params)

		if @click.save
			respond_to do |format|
	        	format.html { redirect_to @click, notice: 'Click was successfully created.'}
	        	format.json { render json: @click, status: 200}
	    	end
		else
			respond_to do |format|
				format.json { render status: 422}
	        	format.html { render :new}
	    	end	
		end
	end


	def destroy
	    if @click.destroy      
	    	respond_to do |format|
	        	format.html {redirect_to @click, notice: 'Click was successfully destroyed.'}
	        	format.json { head 204}
	    	end
	    else
	    	respond_to do |format|
	    		format.html { redirect_to :back, notice: "Click wasn't destroyed"}
	    		format.json { render json: @click.errors, status: :unprocessable_entity }
	    	end
	    end
	end

	private

	  def click_params
    	params.require(:click).permit(:clickable_id, :clickable_type)
  	end 

  	def fetch_user
  		@user = User.find(params[:user_id])
  	end


  	def fetch_click
  		@click = Click.find(params[:id])
  	end



end
