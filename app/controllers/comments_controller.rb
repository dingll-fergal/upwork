class CommentsController < ApplicationController
	before_filter :authenticate_user!
	before_filter :fetch_comment, except: [:index, :create, :new]
	before_filter :authenticate_owner, only: [:update, :destroy]
	before_filter :fetch_object, only: [:index, :new]



	def new
		@comment = Comment.new

		respond_to do |format|
        	format.html
        	format.js
    	end
	end

	def index

		if @object
			# Retrieve all comments belonging to @object ordered by params[:order] or created_at
			@comments = @object.comments.order(params[:order] || "created_at desc")
		else
			# Retrieve all comments ordered by params[:order] or created_at
			@comments = Comment.all.order(params[:order] || "created_at desc")
		end

		# Pagination
		if params[:page]
			@comments = @comments.paginate(:page => params[:page])
		end

		# Add :distance_in_time field to all elements of @comments collection
		if params[:granularity]
			@comments = @comments.calculate_distance_in_time(params[:granularity])
		end
		
		respond_to do |format|
        	format.html
        	format.js
        	format.json { render json: @comments, status: 200 }
    	end
	end


	def create
		@comment = current_user.comments.new(comment_params)

		if @comment.save
			respond_to do |format|
	        	format.html { redirect_to @comment.object, notice: 'Comment was successfully created.'}
	        	format.json { render json: @comment, status: 200}
	        	format.js
	    	end
		else
			respond_to do |format|
				format.json { render status: 422}
	        	format.html { render :new}
	        	format.js
	    	end	
		end
	end

	def update	
		if @comment.update(comment_params)
			respond_to do |format|
	        	format.html { redirect_to :back, notice: 'Comment was successfully updated.'}
	        	format.json { render json: @comment, status: 200}
	    	end
		else
			respond_to do |format|
	        	format.html { render :back}
	        	format.json { render json: @comment.errors, status: 422 }
	    	end			
		end
	end

	def destroy
	    if @comment.destroy      
	    	respond_to do |format|
	        	format.html {redirect_to :back, notice: 'Comment was successfully destroyed.'}
	        	format.json { head 204}
	    	end
	    else
	    	respond_to do |format|
	    		format.html { redirect_to :back, notice: "Comment wasn't destroyed"}
	    		format.json { render json: @comment.errors, status: :unprocessable_entity }
	    	end
	    end
	end

	private



	def authenticate_owner
		block_access unless (@comment.user == current_user) || current_user.admin? || current_user.super_user?
	end

	def comment_params
    	params.require(:comment).permit(:commentable_id, :commentable_type, :content)
  	end 

  	def fetch_comment
  		@comment = Comment.find(params[:id])
  	end

end
