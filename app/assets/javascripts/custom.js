 WebFontConfig = {
    google: { families: [ 'Roboto:400,400italic,500,500italic,700,700italic,100,100italic,300,300italic,900,900italic:latin' ] }
  };
  (function() {
    var wf = document.createElement('script');
    wf.src = 'https://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
    wf.type = 'text/javascript';
    wf.async = 'true';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(wf, s);
  })();


  $(document).ready(function(){
  
  $('ul.tabs li').click(function(){
    var tab_id = $(this).attr('data-tab');

    $('ul.tabs li').removeClass('current');
    $('.tab-content').removeClass('current');

    $(this).addClass('current');
    $("#"+tab_id).addClass('current');
  });



});

  $(document).ready(function(){
 
  $('.innertabs li').click(function(){
    var tab_id = $(this).attr('data-tab');

    $('.innertabs li').removeClass('active-tab');
    $('.tab-content').removeClass('active-tab');

    $(this).addClass('active-tab');
    $("#"+tab_id).addClass('active-tab');
  });

});

 $('.article-options-wrap li').on('click', function () {
  var divID = $(this).attr('data-box');
  $(this).addClass('option-active').siblings().removeClass('option-active');
  $('#' + divID).addClass('option-active').siblings().removeClass('option-active');
});

$('.comment-options .ul-list a div').click(function(e) {
    e.preventDefault();
    $('.comment-options .ul-list a div').removeClass('active-social-menu');
    $(this).addClass('active-social-menu');
});


