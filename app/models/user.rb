class User < ActiveRecord::Base
  # Fields: ["id", "email", "encrypted_password", "reset_password_token", "reset_password_sent_at", "remember_created_at", "sign_in_count", "first_name", "last_name", "authenticated", "role", "enabled", "avatar_file_name", "avatar_content_type", "avatar_file_size", "avatar_updated_at", "created_at", "updated_at"]
	OAUTH_PROVIDERS = [:google_oauth2, :facebook, :twitter]

	devise :database_authenticatable, :registerable, :recoverable, :rememberable, :validatable,
				 :omniauthable, :omniauth_providers => OAUTH_PROVIDERS

	has_attached_file :avatar, :default_url => ActionController::Base.helpers.asset_path('missing_user.png')
	validates_attachment_content_type :avatar, :content_type => [/\Aimage\/.*\Z/]

	has_many :clicks
	has_many :comments
	has_many :level1s
	has_many :level2s
	has_many :level3s
	has_many :favourites

	validates :email, :presence => true
	validates :role, :presence => true
	
	after_create :check_authentication_mode
  acts_as_messageable

  # <overview> Does this user follow an object
  # <param> object [Level1 || Level2 || Level3]
  # <return> [Boolean] 
	def follow? object
		favourites.where(favouritable_type: object.class.to_s, favouritable_id: object.id).exists?
	end

  # <overview> Full name of the user
  # <return> [String]
  def name
    return "no name" unless first_name || last_name
    (first_name || "") + " " + (last_name || "")
  end

  def mailboxer_name
    return email unless first_name || last_name
    (first_name || "") + " " + (last_name || "")
  end

  def mailboxer_email(object)
    self.email
  end

  # <overview> Does user belong to super users level1
  # <return> [Boolean]
	def super_user?
		role == "super"
	end

  # <overview> Does user belong to admins level1
  # <return> [Boolean]
	def admin?
		(role == "admin") || (email == "admin@admin.com")
	end

	def active_for_authentication?
   	super and self.enabled and (self.authenticated != false)
  end


  # <overview> Message shown to user after authentication if user's account has been disabled or user hasn't been authenticated
	def inactive_message
		if enabled 
			if authenticated
				super
			else
				"Admin has to manually authenticate your account"
			end
		else
			"Your account has been disabled by admin"
		end
	end

	# Create new user from OAUTH provider
	def self.from_omniauth(auth)
		where(provider: auth.provider, uid: auth.uid).first_or_create do |user|
			user.email = auth.info.email
			user.password = Devise.friendly_token[0,20]
			user.first_name = auth.info.name.split(' ',2)[0] || ''   # assuming the user model has a first name
			user.last_name = auth.info.name.split(' ',2)[1] || ''  # assuming the user model has a last name
			user.image = auth.info.image # assuming the user model has an image
		end
	end

  private

  # Modify default value of :authenticated field depending on global settings
	def check_authentication_mode
		if Setting.mode == "open"
			update(authenticated: true)
		else
			update(authenticated: false)
		end
	end


end
