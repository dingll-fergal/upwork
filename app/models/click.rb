class Click < ActiveRecord::Base
	# Click(id: integer, user_id: integer, clickable_id: integer, clickable_type: string, created_at: datetime, updated_at: datetime)

	belongs_to :clickable, polymorphic: true
	belongs_to :user

	# <return> [Level1, Level2, Level3]
	def object
		clickable_type.constantize.find(clickable_id)
	end

end
