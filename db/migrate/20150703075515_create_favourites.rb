class CreateFavourites < ActiveRecord::Migration
  def change
    create_table :favourites do |t|
    	t.references :user, null: false
    	t.references :favouritable, polymorphic: true, index: true

    	t.timestamps null: false
    end
  end
end
