class CreateRounds < ActiveRecord::Migration
  def change
    create_table :rounds do |t|
      t.references :competition, index: true
      t.string :title
      t.datetime :start_at
      t.datetime :end_at
      t.integer :num
      t.timestamps null: false
    end
  end
end
