class Level2sController < ApplicationController

	before_filter :authenticate_user!
	before_action :fetch_level1
	before_filter :fetch_level2, only: [:show, :edit, :update, :destroy, :add_level1, :remove]
	before_filter :authenticate_owner, only: [:edit, :update, :destroy, :remove, :add_level1]


	# GET /level2s.(js|html|json)
	# GET /level1/:level1_id/level2s.(js|html|json)
	def index
		if @level1
			# Retrieve all visible level1's level2s whose name contain params[:query] ordered by params[:order] or date
			@level2s = @level1.level2s.search(params[:query]).order(params[:order] || "date desc")
		else
			# Retrieve all visible level2s whose name contain params[:query] ordered by params[:order] or date
			@level2s = Level2.search(params[:query]).order(params[:order] || "date desc")
		end

		# Filter by id
		if params[:filter]
			@level2s = @level2s.where(filters)
		end

		# Pagination
		if params[:page]
			@level2s = @level2s.visible.paginate(:page => params[:page])
		end

		# Add :distance_in_time field to all elements of @level2s collection
		if params[:granularity]
			@level2s = @level2s.calculate_distance_in_time(params[:granularity])
		end



		respond_to do |format|
        	format.html {
        		authenticate_admin unless @level1 && @level1.user == current_user
        	}
        	format.js
        	format.json { render json: @level2s, status: 200 }
    	end
	end

	# GET /level2s/:id.(html|json)
	def show
		@match = @level2.match
		respond_to do |format|
        	format.html
        	format.js
        	format.json { render json: @level2, status: 200, location: @level2}
    	end
	end

	# GET /level1s/:level1_id/level2s/:id.(html|js)
	def new
		@level2 = Level2.new
		respond_to do |format|
			format.js
        	format.html
    	end	
	end

	# POST  /level1s/:level1_id/level2s.(html|js|json)
	def create
		@level2 = @level1.level2s.build(level2_params)
		@level2.user = current_user

		if @level1.save
			respond_to do |format|
				format.js
	        	format.html { redirect_to [@level1, @level2], notice: 'Level2 was successfully created.'}
	        	format.json { render json: @level2, status: 200}
	    	end
		else
			respond_to do |format|
				format.js
				format.json { render status: 422}
	        	format.html { render :new}
	    	end	
		end
	end

	# GET /level2s/:id/edit.(html|js)
	def edit
		respond_to do |format|
			format.js
        	format.html
    	end	
	end

	# PUT /level2s/:id.(html|js|json)
	def update	
		if @level2.update(level2_params)
			respond_to do |format|
				format.js
	        	format.html { redirect_to (@level1 ? [@level1, @level2] : @level2), notice: 'Level2 was successfully updated.'}
	        	format.json { render json: @level2, status: 200}
	    	end
		else
			respond_to do |format|
				format.js
	        	format.html { render :edit}
	        	format.json { render json: @level2.errors, status: 422 }
	    	end			
		end
	end

	# DELETE /level2s/:id.(html|json)
	def destroy
	    if @level2.destroy
	    	respond_to do |format|
	        	format.html {redirect_to @level1 ? level1_level2s_path(@level1) : level2s_path, notice: 'Level2 was successfully destroyed.'}
	        	format.json { head 204}
	    	end
	    else
	    	respond_to do |format|
	    		format.html { redirect_to :back, alert: "Level2 wasn't destroyed"}
	    		format.json { render json: @level2.errors, status: :unprocessable_entity }
	    	end
	    end
	end

	# Delete level2 from @level1
	# PATCH /level1s/:level1_id/level2s/:id/remove
	def remove
		if @level1.level2s.delete @level2
			redirect_to @level2, notice: "Level2 no longer belongs to the level1"
		else
			redirect_to :back, alert: "Level1 wasn't deleted"
		end
	end

	# Add level2 to @level1
	# PATCH /level2s/:id/add_level1
	def add_level1
		if params[:level2] && params[:level2][:level1_id]
			level1 = Level1.find(params[:level2][:level1_id])
			level1.level2s << @level2
			redirect_to [level1, @level2], notice: "Level2 was added to the level1"
		else
			redirect_to :back, alert: "Level2 wasn't added to the level1"
		end
	end


	private


	def authenticate_owner
		block_access unless (@level2.user == current_user) || current_user.admin? || current_user.super_user?
	end

	def level2_params
    	params.require(:level2).permit(:name, :visible, :level1_id, :date, :avatar, :description, :list_price, :discounted_price)
  	end

  	def fetch_level1
  		@level1 = params[:level1_id] ? Level1.find(params[:level1_id]) : nil
  	end 

  	def fetch_level2
  		@level2 = Level2.find(params[:id])
  	end

end