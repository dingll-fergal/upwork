class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments do |t|
    	t.string :content, limit: 255
    	t.references :user, null: false
    	t.references :commentable, polymorphic: true, index: true

    	t.timestamps null: false
    end
  end
end
