class CreateLevel1sLevel2s < ActiveRecord::Migration
  def change
    create_table :level1s_level2s, :id => false do |t|
    	t.references :level2, index: true, null: false
        t.references :level1, index: true, null: false
    end
  end
end
