class Season < ActiveRecord::Base
  has_many :competitions, dependent: :destroy
end
