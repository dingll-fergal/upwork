class Competition < ActiveRecord::Base
  belongs_to :league
  belongs_to :season

  has_many :teams, dependent: :destroy
  has_many :groups, dependent: :destroy
  has_many :rounds, dependent: :destroy

end
