class CreateLevel3s < ActiveRecord::Migration
  def change
    create_table :level3s do |t|
    	t.string :name, limit: 40
    	t.boolean :visible, default: false
    	t.references :user, null: false, index: true
    	t.references :level2, null: false, index: true
     	t.datetime :date, index: true, :null => false, :default => Time.now
     	t.attachment :avatar
     	t.text :description
        t.string :slug, uniq: true, index: true

    	t.timestamps null: false
    end
  end
end
