class Level1 < ActiveRecord::Base
  # ["id", "name", "visible", "user_id", "date", "avatar_file_name", "avatar_content_type", "avatar_file_size", "avatar_updated_at", "description", "created_at", "updated_at"]

  include Resourceable
  extend FriendlyId
  friendly_id :name, use: [:history, :finders]

  scope :visible, -> { where(visible: true) }

  belongs_to :user
  has_many :comments, as: :commentable, dependent: :destroy
  has_many :clicks, as: :clickable, dependent: :destroy
  #has_many :resources, as: :mediable, dependent: :destroy
  has_many :descriptions, as: :descriptable, dependent: :destroy
  has_many :favourites, as: :favouritable, dependent: :destroy

  has_and_belongs_to_many :level2s

  validates :name, :presence => true
  validates_length_of :name, :maximum => 40

  # validates :description, uniqueness: true, :allow_blank => true

  validates :user, :presence => true

  has_attached_file :avatar
  validates_attachment_content_type :avatar, :content_type => [/\Aimage\/.*\Z/]

  attr_accessor :distance_in_time

  after_save :add_description
  after_destroy :clear_level2s

  before_destroy {self.level2s.clear}


  self.per_page = 20

  # <overview> Helper method for timelines
  # <return> nil
  def parentID
    nil
  end

  def self.child_name
    "level2"
  end

  def self.display_date date, granularity
    case granularity
      when "minute"
        return date.strftime("%I:%M%p")
      when "day"
        return date.strftime("%B %d, %Y")
      when "month"
        return date.strftime("%B %Y")
      when "year"
        return date.strftime("%Y")
      when "decade"
        return date.strftime("%Y")
    end
  end


  def self.group_level2s_by_date(granularity, order = "desc")
    groups = {}
    all.each do |level1|
      level1.level2s.order("date desc").each do |level2|
        group = groups[display_date(level2.date, granularity)]
        (group ||= {})[:date] = level2.date

        unless (group ||= {})[:level2s]
          all.pluck(:id).each { |level1_id| (group[:level2s] ||= {})[level1_id] = [] }
        end

        group[:level2s][level1.id] << level2
        groups[display_date(level2.date, granularity)] = group
      end
    end

    first_date = nil

    if order == "asc"
      groups = groups.sort_by { |k, v| v[:date] }.to_h

    else
      groups = groups.sort_by { |k, v| v[:date] }.reverse.to_h
    end

    groups.each_pair do |key, group|

      group[:difference] = first_date ? self.time_distance(first_date, group[:date], granularity) : 0

      group[:difference] = -group[:difference] if order == "asc"

      first_date = group[:date]
    end


    return groups
  end

  # <overview> Delete all related level2s except whose which belong to some other level1
  def clear_level2s
    level2s.each { |level2| level2.destroy unless level2.level1s.any? }
  end

  # <overview>  Add :distance_in_time to objects from collection
  # <return> ActiveRecord Collection
  scope :calculate_distance_in_time, lambda { |granularity=nil|
    time = nil
    all.each do |object|

      this_time = object.date
      object.distance_in_time = time ? time_distance(time, this_time, granularity) : 0.01

      time = this_time
    end
  }

  # <overview> Distance between two dates
  # <param> [Datetime] First date
  # <param> [Datetime] Second date
  # <param> [String] Granularity
  # <return>	[Integer]
  def self.time_distance(a, b, granularity)
    return 0 unless granularity

    case granularity
      when "minute"
        distance = (a.day - b.day) * 60 * 24 + (a.hour - b.hour) * 60 * (a.minute - b.minute)
      when "day"
        distance = (a.year - b.year) * 365 + (a.month - b.month) * 30 + (a.day - b.day)
      when "month"
        distance = (a.year - b.year) * 12 + (a.month - b.month)
      when "year"
        distance = a.year - b.year
      when "decade"
        distance = (a.year - b.year) / 10
    end
  end

  # <overview> All objects whose name contains query
  # <param> [String] query
  # <return>	ActiveRecord Collection
  def self.search query = nil
    result = all
    if query
      result = joins("LEFT OUTER JOIN level1s_level2s ON level1s.id = level1s_level2s.level1_id 
                      LEFT OUTER JOIN level2s ON level2s.id = level1s_level2s.level2_id")
              .where("LOWER(level1s.name) LIKE :q OR LOWER(level1s.description) LIKE :q OR LOWER(level2s.name) LIKE :q OR LOWER(level2s.description) LIKE :q", q: "%#{query.downcase}%", q: "%#{query.downcase}%", q: "%#{query.downcase}%", q: "%#{query.downcase}%")
              .group("level1s.id")
    end
    return result
  end

  # return all levels 1 where search criteria matches with lelev 2 or three
  # this method can be refactor and can get data by a single SQL or minimum SQL
  def self.get_maching_records(query, search)
    results = self.search query
    return results if results.blank? || search.blank? || (query.downcase == search.downcase)
    level2s_ids = []
    results.each do |level1|
      level2s_ids << level1.level2s.where("LOWER(level2s.name) LIKE :q", q: "%#{search.downcase}%").pluck(:id)
      level2s_ids << level1.level2s.joins(:level3s).where("LOWER(level3s.name) LIKE :q", q: "%#{search.downcase}%").pluck(:id)
    end
    results = Level1.joins(:level2s).where("level2s.id IN(?)", level2s_ids.flatten.uniq)
    results
  end

  # <overview> Update history of description changes
  def add_description
    if descriptions.any?
      if descriptions.last.content != description
        descriptions.create(content: description)
      end
    else
      descriptions.create(content: description)
    end
  end

  #show on expansive view top fotos based on clicks
  def load_level2s_photos
    level2_ids = level2s.pluck(:id)
    return [] if level2_ids.blank?
    top_fifty_resurces = Resource.select("resources.*, count(clicks.id) as click_counts")
            .joins( "INNER JOIN clicks ON clicks.clickable_id = resources.id AND clicks.clickable_type='Resource'" )
            .where("(resources.mediable_id IN (?) AND resources.mediable_type='Level2')", level2_ids)
            .group("resources.id")
            .order("click_counts DESC").limit(50)
    if (!top_fifty_resurces.blank? && !(level2_ids - top_fifty_resurces.map(&:mediable_id).uniq).blank?)
      other_resources = Resource.select("resources.*")
            .where("(resources.mediable_id IN (?) AND resources.mediable_type='Level2')", level2_ids - top_fifty_resurces.map(&:mediable_id).uniq)
            .group("resources.mediable_id")
      top_fifty_resurces = top_fifty_resurces + other_resources if other_resources
    elsif top_fifty_resurces.blank?
      top_fifty_resurces = Resource.select("resources.*")
            .where("(resources.mediable_id IN (?) AND resources.mediable_type='Level2')", level2_ids)
            .group("resources.mediable_id")     
    end
    top_fifty_resurces
  end

end
