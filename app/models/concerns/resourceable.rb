module Resourceable
  extend ActiveSupport::Concern

  included do
    has_many :resources, as: :mediable, dependent: :destroy
  end

  #user photos
  def user_uploads
    resources.where(professional: false)
  end

  def professional_uploads
    resources.where(professional: true)
  end
end
