class League < ActiveRecord::Base
  has_many :competitions, dependent: :destroy

end
