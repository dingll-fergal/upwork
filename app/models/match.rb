class Match < ActiveRecord::Base
  belongs_to :team
  belongs_to :group
  belongs_to :round
  belongs_to :level2

  has_many :team_matches, dependent: :destroy
  has_many :teams, through: :team_matches
  has_one :news_article


  def first_team
    self.teams.first
  end

  def second_team
    self.teams.last
  end

  def first_team_name
    first_team.title
  end

  def second_team_name
    second_team.title
  end

  def first_team_score
    self.team_matches.find_by_team_id(first_team.id).score
  end

  def second_team_score
    self.team_matches.find_by_team_id(second_team.id).score
  end

  def first_team_players
    first_team.players
  end

  def second_team_players
    second_team.players
  end

end
