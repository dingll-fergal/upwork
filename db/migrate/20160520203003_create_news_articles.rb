class CreateNewsArticles < ActiveRecord::Migration
  def change
    create_table :news_articles do |t|
      t.string :title
      t.text :body
      t.references :match
      t.references :level2
      t.timestamps null: false
    end
  end
end
