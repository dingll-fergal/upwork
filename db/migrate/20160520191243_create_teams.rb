class CreateTeams < ActiveRecord::Migration
  def change
    create_table :teams do |t|
      t.references :competition, index: true
      t.string :title
      t.string :code
      t.string :address
      t.string :web
      t.integer :num

      t.timestamps null: false
    end
  end
end
