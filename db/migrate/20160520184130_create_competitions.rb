class CreateCompetitions < ActiveRecord::Migration
  def change
    create_table :competitions do |t|
      t.string :title
      t.datetime :start_at
      t.datetime :end_at
      t.integer :num
      t.references :league
      t.references :season

      t.timestamps null: false
    end
  end
end
