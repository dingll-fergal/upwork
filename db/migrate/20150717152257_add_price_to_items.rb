class AddPriceToItems < ActiveRecord::Migration
  def change
  	add_column :level1s, :list_price, :float
  	add_column :level1s, :discounted_price, :float
  	add_column :level2s, :list_price, :float
  	add_column :level2s, :discounted_price, :float
   	add_column :level3s, :list_price, :float
  	add_column :level3s, :discounted_price, :float
  end
end
