class CreateSquads < ActiveRecord::Migration
  def change
    create_table :squads do |t|
      t.references :team, index: true
      t.references :player, index: true
      t.string :pos
      t.integer :num

      t.timestamps null: false
    end
  end
end
