# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160621103452) do

  create_table "ckeditor_assets", force: :cascade do |t|
    t.string   "data_file_name",    limit: 255, null: false
    t.string   "data_content_type", limit: 255
    t.integer  "data_file_size",    limit: 4
    t.integer  "assetable_id",      limit: 4
    t.string   "assetable_type",    limit: 30
    t.string   "type",              limit: 30
    t.integer  "width",             limit: 4
    t.integer  "height",            limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "ckeditor_assets", ["assetable_type", "assetable_id"], name: "idx_ckeditor_assetable", using: :btree
  add_index "ckeditor_assets", ["assetable_type", "type", "assetable_id"], name: "idx_ckeditor_assetable_type", using: :btree

  create_table "clicks", force: :cascade do |t|
    t.integer  "user_id",        limit: 4,   null: false
    t.integer  "clickable_id",   limit: 4
    t.string   "clickable_type", limit: 255
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  add_index "clicks", ["clickable_type", "clickable_id"], name: "index_clicks_on_clickable_type_and_clickable_id", using: :btree

  create_table "comments", force: :cascade do |t|
    t.string   "content",          limit: 255
    t.integer  "user_id",          limit: 4,   null: false
    t.integer  "commentable_id",   limit: 4
    t.string   "commentable_type", limit: 255
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
  end

  add_index "comments", ["commentable_type", "commentable_id"], name: "index_comments_on_commentable_type_and_commentable_id", using: :btree

  create_table "competitions", force: :cascade do |t|
    t.string   "title",      limit: 255
    t.datetime "start_at"
    t.datetime "end_at"
    t.integer  "num",        limit: 4
    t.integer  "league_id",  limit: 4
    t.integer  "season_id",  limit: 4
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "descriptions", force: :cascade do |t|
    t.text     "content",           limit: 65535
    t.integer  "descriptable_id",   limit: 4
    t.string   "descriptable_type", limit: 255
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
  end

  add_index "descriptions", ["descriptable_type", "descriptable_id"], name: "index_descriptions_on_descriptable_type_and_descriptable_id", using: :btree

  create_table "expansives", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "favourites", force: :cascade do |t|
    t.integer  "user_id",           limit: 4,   null: false
    t.integer  "favouritable_id",   limit: 4
    t.string   "favouritable_type", limit: 255
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  add_index "favourites", ["favouritable_type", "favouritable_id"], name: "index_favourites_on_favouritable_type_and_favouritable_id", using: :btree

  create_table "friendly_id_slugs", force: :cascade do |t|
    t.string   "slug",           limit: 255, null: false
    t.integer  "sluggable_id",   limit: 4,   null: false
    t.string   "sluggable_type", limit: 50
    t.string   "scope",          limit: 255
    t.datetime "created_at"
  end

  add_index "friendly_id_slugs", ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true, using: :btree
  add_index "friendly_id_slugs", ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type", using: :btree
  add_index "friendly_id_slugs", ["sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_id", using: :btree
  add_index "friendly_id_slugs", ["sluggable_type"], name: "index_friendly_id_slugs_on_sluggable_type", using: :btree

  create_table "goals", force: :cascade do |t|
    t.integer  "player_id",  limit: 4
    t.integer  "match_id",   limit: 4
    t.string   "minute",     limit: 255
    t.boolean  "offset",     limit: 1
    t.boolean  "penalty",    limit: 1,   default: false
    t.boolean  "ownapal",    limit: 1,   default: false
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
  end

  add_index "goals", ["match_id"], name: "index_goals_on_match_id", using: :btree
  add_index "goals", ["player_id"], name: "index_goals_on_player_id", using: :btree

  create_table "groups", force: :cascade do |t|
    t.integer  "competition_id", limit: 4
    t.string   "title",          limit: 255
    t.integer  "num",            limit: 4
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  add_index "groups", ["competition_id"], name: "index_groups_on_competition_id", using: :btree

  create_table "leagues", force: :cascade do |t|
    t.string   "title",      limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "level1s", force: :cascade do |t|
    t.string   "name",                limit: 40
    t.boolean  "visible",             limit: 1,     default: false
    t.integer  "user_id",             limit: 4,                                     null: false
    t.datetime "date",                              default: '2016-04-25 09:07:19', null: false
    t.string   "avatar_file_name",    limit: 255
    t.string   "avatar_content_type", limit: 255
    t.integer  "avatar_file_size",    limit: 4
    t.datetime "avatar_updated_at"
    t.text     "description",         limit: 65535
    t.string   "slug",                limit: 255
    t.datetime "created_at",                                                        null: false
    t.datetime "updated_at",                                                        null: false
    t.float    "list_price",          limit: 24
    t.float    "discounted_price",    limit: 24
    t.integer  "click_count",         limit: 4
  end

  add_index "level1s", ["date"], name: "index_level1s_on_date", using: :btree
  add_index "level1s", ["slug"], name: "index_level1s_on_slug", using: :btree
  add_index "level1s", ["user_id"], name: "index_level1s_on_user_id", using: :btree

  create_table "level1s_level2s", id: false, force: :cascade do |t|
    t.integer "level2_id", limit: 4, null: false
    t.integer "level1_id", limit: 4, null: false
  end

  add_index "level1s_level2s", ["level1_id"], name: "index_level1s_level2s_on_level1_id", using: :btree
  add_index "level1s_level2s", ["level2_id"], name: "index_level1s_level2s_on_level2_id", using: :btree

  create_table "level2s", force: :cascade do |t|
    t.string   "name",                limit: 40
    t.boolean  "visible",             limit: 1,     default: false
    t.integer  "user_id",             limit: 4,                                     null: false
    t.datetime "date",                              default: '2016-04-25 09:07:20', null: false
    t.string   "avatar_file_name",    limit: 255
    t.string   "avatar_content_type", limit: 255
    t.integer  "avatar_file_size",    limit: 4
    t.datetime "avatar_updated_at"
    t.text     "description",         limit: 65535
    t.string   "slug",                limit: 255
    t.datetime "created_at",                                                        null: false
    t.datetime "updated_at",                                                        null: false
    t.float    "list_price",          limit: 24
    t.float    "discounted_price",    limit: 24
    t.integer  "click_count",         limit: 4
  end

  add_index "level2s", ["date"], name: "index_level2s_on_date", using: :btree
  add_index "level2s", ["slug"], name: "index_level2s_on_slug", using: :btree
  add_index "level2s", ["user_id"], name: "index_level2s_on_user_id", using: :btree

  create_table "level3s", force: :cascade do |t|
    t.string   "name",                limit: 40
    t.boolean  "visible",             limit: 1,     default: false
    t.integer  "user_id",             limit: 4,                                     null: false
    t.integer  "level2_id",           limit: 4,                                     null: false
    t.datetime "date",                              default: '2016-04-25 09:07:20', null: false
    t.string   "avatar_file_name",    limit: 255
    t.string   "avatar_content_type", limit: 255
    t.integer  "avatar_file_size",    limit: 4
    t.datetime "avatar_updated_at"
    t.text     "description",         limit: 65535
    t.string   "slug",                limit: 255
    t.datetime "created_at",                                                        null: false
    t.datetime "updated_at",                                                        null: false
    t.float    "list_price",          limit: 24
    t.float    "discounted_price",    limit: 24
  end

  add_index "level3s", ["date"], name: "index_level3s_on_date", using: :btree
  add_index "level3s", ["level2_id"], name: "index_level3s_on_level2_id", using: :btree
  add_index "level3s", ["slug"], name: "index_level3s_on_slug", using: :btree
  add_index "level3s", ["user_id"], name: "index_level3s_on_user_id", using: :btree

  create_table "mailboxer_conversation_opt_outs", force: :cascade do |t|
    t.integer "unsubscriber_id",   limit: 4
    t.string  "unsubscriber_type", limit: 255
    t.integer "conversation_id",   limit: 4
  end

  add_index "mailboxer_conversation_opt_outs", ["conversation_id"], name: "index_mailboxer_conversation_opt_outs_on_conversation_id", using: :btree
  add_index "mailboxer_conversation_opt_outs", ["unsubscriber_id", "unsubscriber_type"], name: "index_mailboxer_conversation_opt_outs_on_unsubscriber_id_type", using: :btree

  create_table "mailboxer_conversations", force: :cascade do |t|
    t.string   "subject",    limit: 255, default: ""
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  create_table "mailboxer_notifications", force: :cascade do |t|
    t.string   "type",                 limit: 255
    t.text     "body",                 limit: 65535
    t.string   "subject",              limit: 255,   default: ""
    t.integer  "sender_id",            limit: 4
    t.string   "sender_type",          limit: 255
    t.integer  "conversation_id",      limit: 4
    t.boolean  "draft",                limit: 1,     default: false
    t.string   "notification_code",    limit: 255
    t.integer  "notified_object_id",   limit: 4
    t.string   "notified_object_type", limit: 255
    t.string   "attachment",           limit: 255
    t.datetime "updated_at",                                         null: false
    t.datetime "created_at",                                         null: false
    t.boolean  "global",               limit: 1,     default: false
    t.datetime "expires"
  end

  add_index "mailboxer_notifications", ["conversation_id"], name: "index_mailboxer_notifications_on_conversation_id", using: :btree
  add_index "mailboxer_notifications", ["notified_object_id", "notified_object_type"], name: "index_mailboxer_notifications_on_notified_object_id_and_type", using: :btree
  add_index "mailboxer_notifications", ["sender_id", "sender_type"], name: "index_mailboxer_notifications_on_sender_id_and_sender_type", using: :btree
  add_index "mailboxer_notifications", ["type"], name: "index_mailboxer_notifications_on_type", using: :btree

  create_table "mailboxer_receipts", force: :cascade do |t|
    t.integer  "receiver_id",     limit: 4
    t.string   "receiver_type",   limit: 255
    t.integer  "notification_id", limit: 4,                   null: false
    t.boolean  "is_read",         limit: 1,   default: false
    t.boolean  "trashed",         limit: 1,   default: false
    t.boolean  "deleted",         limit: 1,   default: false
    t.string   "mailbox_type",    limit: 25
    t.datetime "created_at",                                  null: false
    t.datetime "updated_at",                                  null: false
  end

  add_index "mailboxer_receipts", ["notification_id"], name: "index_mailboxer_receipts_on_notification_id", using: :btree
  add_index "mailboxer_receipts", ["receiver_id", "receiver_type"], name: "index_mailboxer_receipts_on_receiver_id_and_receiver_type", using: :btree

  create_table "matches", force: :cascade do |t|
    t.integer  "group_id",   limit: 4
    t.integer  "round_id",   limit: 4
    t.integer  "level2_id",  limit: 4
    t.integer  "num",        limit: 4
    t.datetime "start_at"
    t.datetime "fulltime"
    t.string   "play_at",    limit: 255
    t.boolean  "knockout",   limit: 1
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "matches", ["group_id"], name: "index_matches_on_group_id", using: :btree
  add_index "matches", ["level2_id"], name: "index_matches_on_level2_id", using: :btree
  add_index "matches", ["round_id"], name: "index_matches_on_round_id", using: :btree

  create_table "news_articles", force: :cascade do |t|
    t.string   "title",      limit: 255
    t.text     "body",       limit: 65535
    t.integer  "match_id",   limit: 4
    t.integer  "level2_id",  limit: 4
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  create_table "players", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.integer  "num",        limit: 4
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "resources", force: :cascade do |t|
    t.string   "content_file_name",    limit: 255
    t.string   "content_content_type", limit: 255
    t.integer  "content_file_size",    limit: 4
    t.datetime "content_updated_at"
    t.integer  "mediable_id",          limit: 4
    t.string   "mediable_type",        limit: 255
    t.string   "avatar_file_name",     limit: 255
    t.string   "avatar_content_type",  limit: 255
    t.integer  "avatar_file_size",     limit: 4
    t.datetime "avatar_updated_at"
    t.text     "description",          limit: 65535
    t.string   "video_url",            limit: 255
    t.datetime "created_at",                                         null: false
    t.datetime "updated_at",                                         null: false
    t.boolean  "professional",         limit: 1,     default: false
    t.string   "image_file_name",      limit: 255
    t.string   "image_content_type",   limit: 255
    t.integer  "image_file_size",      limit: 4
    t.datetime "image_updated_at"
  end

  add_index "resources", ["mediable_type", "mediable_id"], name: "index_resources_on_mediable_type_and_mediable_id", using: :btree

  create_table "rounds", force: :cascade do |t|
    t.integer  "competition_id", limit: 4
    t.string   "title",          limit: 255
    t.datetime "start_at"
    t.datetime "end_at"
    t.integer  "num",            limit: 4
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  add_index "rounds", ["competition_id"], name: "index_rounds_on_competition_id", using: :btree

  create_table "seasons", force: :cascade do |t|
    t.string   "title",      limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "settings", force: :cascade do |t|
    t.string "mode", limit: 255, default: "open"
  end

  create_table "squads", force: :cascade do |t|
    t.integer  "team_id",    limit: 4
    t.integer  "player_id",  limit: 4
    t.string   "pos",        limit: 255
    t.integer  "num",        limit: 4
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "squads", ["player_id"], name: "index_squads_on_player_id", using: :btree
  add_index "squads", ["team_id"], name: "index_squads_on_team_id", using: :btree

  create_table "team_matches", force: :cascade do |t|
    t.integer "team_id",  limit: 4
    t.integer "match_id", limit: 4
    t.integer "score",    limit: 4
    t.boolean "winner",   limit: 1
  end

  add_index "team_matches", ["match_id"], name: "index_team_matches_on_match_id", using: :btree
  add_index "team_matches", ["team_id"], name: "index_team_matches_on_team_id", using: :btree

  create_table "teams", force: :cascade do |t|
    t.integer  "competition_id", limit: 4
    t.string   "title",          limit: 255
    t.string   "code",           limit: 255
    t.string   "address",        limit: 255
    t.string   "web",            limit: 255
    t.integer  "num",            limit: 4
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  add_index "teams", ["competition_id"], name: "index_teams_on_competition_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "email",                  limit: 255,   default: "",     null: false
    t.string   "encrypted_password",     limit: 255,   default: "",     null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,     default: 0,      null: false
    t.string   "first_name",             limit: 255
    t.string   "last_name",              limit: 255
    t.boolean  "authenticated",          limit: 1
    t.string   "role",                   limit: 255,   default: "user", null: false
    t.boolean  "enabled",                limit: 1,     default: true
    t.string   "avatar_file_name",       limit: 255
    t.string   "avatar_content_type",    limit: 255
    t.integer  "avatar_file_size",       limit: 4
    t.datetime "avatar_updated_at"
    t.datetime "created_at",                                            null: false
    t.datetime "updated_at",                                            null: false
    t.string   "provider",               limit: 255
    t.string   "uid",                    limit: 255
    t.text     "image",                  limit: 65535
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["provider"], name: "index_users_on_provider", using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  add_index "users", ["uid"], name: "index_users_on_uid", using: :btree

  add_foreign_key "mailboxer_conversation_opt_outs", "mailboxer_conversations", column: "conversation_id", name: "mb_opt_outs_on_conversations_id"
  add_foreign_key "mailboxer_notifications", "mailboxer_conversations", column: "conversation_id", name: "notifications_on_conversation_id"
  add_foreign_key "mailboxer_receipts", "mailboxer_notifications", column: "notification_id", name: "receipts_on_notification_id"
end
