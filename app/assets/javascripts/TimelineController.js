var TimelineController = function (type, manager) {
	var currentElement, currentID, currentPage, timeline, order, granularity;
	var timelineManager = manager;
	var objectType = type;


	initialize();

	// Public method that returns an ID of the selected element
	this.currentID = function(){
		return currentID;
	};

	this.currentElement = function(){
		return currentElement;
	}

	// Public method that returns type of the selected element
	this.objectType = function(){
		return objectType;
	}

	// Public method that returns Jquery DOM element representing this timeline
	this.timeline = function(){
		return timeline;
	}

	// Constructor
	function initialize(){
		// Jquery DOM element representing this timeline
		timeline = $('[data-type=' + objectType + ']');
		currentPage = 2;

		adjustTimelineHeight();
		attachListeners();			
	};	


	function attachListeners(){

		// Activate/Diactivate timeline item on click
		timeline.find(".timeline-block").on("click", function(){
			timelineManager.query = null;

			if ($(this).hasClass("active")){
				deactivate();
			} else {
				activate($(this));
			}
		});	

		// Change display mode on change of select field
		timeline.find("select").on("change", function(){
			changeDisplayMode($(this).val());
		});	

		// Adjust timeline height on scroll and load more elements
		timeline.find(".timeline-body").on('scroll', function() {
			adjustTimelineHeight();
		    if($(this).scrollTop() + $(this).innerHeight() >= this.scrollHeight) {
		        loadMore();
		    }
		});	

		if (objectType == "comment"){
			attachCommentListeners(timeline);
		}
	}

	// Change timeline type (static/dynamic)
	function changeDisplayMode(mode){
		if (mode == "off"){
			granularity = null;
			order = objectType == "comment" ? "created_at desc" : "date desc";
		} else if (mode == "name"){
			granularity = null;
			order = "name asc";
		} else {
			order = objectType == "comment" ? "created_at desc" : "date desc";
			granularity = mode;
		};

		reloadTimeline(function(){
			var element = $('[data-item-id=' + currentID +']');
			activate(element)			
		});

	};

	// Public method for reloadTimeline(callback)
	this.reloadTimeline = function(callback){
		reloadTimeline(callback)
	}
	// Reload all elements from timeline
	function reloadTimeline(callback){
		var data = defaultParams();
		data["page"] = 1;
		loadElements(data, callback);		
	}

	// Default params added to every ajax call 
	function defaultParams(){
		var data = {};
		if (order) {data["order"] = order};
		if (granularity) {data["granularity"] = granularity};

		return data
	}


	// public method for adjustTimelineHeight()
	this.adjustTimelineHeight = function(){
		adjustTimelineHeight();
	};
	// Adjust the height of timeline line
	function adjustTimelineHeight(){
		// var height = timelineHeight();
		// if (timeline.find(".timeline-wrapper").height() > 0){
		// 	margin = timeline.find(".timeline-point").first().offset().top - timeline.find(".timeline-wrapper").offset().top;
		// 	timeline.find(".timeline-line").css("margin-top", margin);
		// }
		// timeline.find(".timeline-line").css("height", height);
	};



	//var margin = $(".favourite-level1-#{level1_id} .compare-level2s .timeline-point").first().offset().top - $(".favourite-level1[data-level1-id=#{level1_id}]").offset().top;

	// Calculate the height of timeline line
	function timelineHeight(){
		if (timeline.find(".timeline-wrapper").height() > 0){
			return timeline.find(".timeline-point").last().offset().top - timeline.find(".timeline-point").first().offset().top
		} else {
			return 0;
		}
	}

	// Public method for reinitialize()
	this.reinitialize = function(){
		reinitialize()
	};

	// Re-attach all listeners
	function reinitialize(){
		timeline.find(".timeline-block").off();
		timeline.find(".timeline-body").off()
		initialize();		
	};

	// attach listeners for comment nodes
	function attachCommentListeners(element){
		// Show input field on click
		element.find(".comment-content").on("click", function(){
			$(this).hide();
			$(this).closest(".comment").find(".comment-input").show();
		});	

		// Save changed on click
		element.find(".comment-input [data-action=save]").on("click", function(){
			var element = $(this);
			$.ajax({ 
				dataType: "json", 
				url: element.closest(".comment-input").data("url"), 
				method: "patch",
				data: { 
					comment: {
						content: element.closest(".comment-input").find("input").val()
					} 
				},
				success: function(data){
					element.closest(".comment").find(".comment-content").text(data.content);
					closeCommentForm(element);
				}
			});
		});

		// Hide input on clicking close button
		element.find(".comment-input [data-action=cancel]").on("click", function(){
			closeCommentForm($(this));
		});
	};

	// Hide input in comment node
	function closeCommentForm(element){
		element.closest(".comment-input").hide();
		element.closest(".comment").find(".comment-content").show();			
	};	

	// Add element to timeline and attach listeners
	this.addElement = function(element) {
		var $element = $(element);
		timeline.find(".timeline-collection").append($element);
		$element.on("click", function(){
			if ($(this).hasClass("active")){
				deactivate();
			} else {
				activate($(this));
			};
		});
	};

	// Build an URL for resourceType
	function resourceURL(memberName, memberId, resourceType){
		if (memberId) {
			return "/" + timelineManager.pluralize(memberName) + "/" + memberId + "/" + resourceType;
		} else {
			return "/" + resourceType;
		}
	};

	// Reload elements for this and children timelines
	function loadElements(data, callback){
		var parent, parentActiveElementID, url;

		url = "/" + timelineManager.pluralize(objectType);

		parent = objectType == "comment" ? timelineManager.currentTimeline : timelineManager.config[objectType]["parent"] 

		if (parent){
			if (parentActiveElementID = timelineManager.config[parent]["activeElementID"]){
				url = resourceURL(parent, parentActiveElementID, timelineManager.pluralize(objectType));
			}
		};

		if (timelineManager.query){
			data["query"] = timelineManager.query;
		};



		$.ajax({ 
			dataType: "script", 
			url: url, 
			data: data,
			success: callback
		});		
	}

	// Load elements from the next page
	function loadMore(){
		var data = defaultParams();
		data["page"] = currentPage;
		loadElements(data, function(){ currentPage += 1; })
	};

	// Build nested URL
	function nestedURL(object){
		return resourceURL(objectType, currentID, timelineManager.pluralize(object))
	}

	this.updateResources = function(element) {
		updateResources(element);
	}

	function updateResources(element){
		currentElement = element;
		currentID = element.data("item-id");
		//console.log(element)
		timelineManager.currentTimeline = objectType;
		timelineManager.config[objectType]["activeElementID"] = currentID;
		loadDescription();
		loadItems("comment");	
	}
	// Activate element on click
	function activate(element){		
		currentElement = element;
		currentID = element.data("item-id");


		timelineManager.currentTimeline = objectType;
		timelineManager.config[objectType]["activeElementID"] = currentID;

		// Scroll the element to timeline top
		scrollToTop();

		if (objectType != "comment"){
			// Change background
			addBackground();
			// Load description, media, and comments
			loadResources();
			// Update URL in browser URL bar
			updateHref();
		}

		// Load parent and children
		loadParent();
		loadChildren();
	};

	function updateHref(){
		if (url = currentURL()){
			history.replaceState(null, null, url);
		}
	}

	// Find url of current element
	function currentURL(){
		var url = "/" + timelineManager.pluralize(objectType) + "/" + currentElement.data("item-name");
		if (currentElement.data("parent-id")){
			var parent = parentTimeline();
			url =  "/" + timelineManager.pluralize(parent.objectType()) + "/" + parent.currentElement().data("item-name") + url;
		}
		return url;
	};

	// Load parrent elemtn
	function loadParent(){
		var parent
		if (parent = parentTimeline()){
			if (!parent.currentID()){
				var parentID = currentElement.data("parent-id");

				$.ajax({ 
					dataType: "script",
					url:  timelineManager.pluralize(parent.objectType()),
					data: {
						"filter": {
							"id": parentID
						},
						"page": 1
					}
				});			
			}
		}
	}

	// Scroll to top of the timeline
	function scrollToTop(){
		if (currentElement && typeof timeline.find(".timeline-body") != undefined && typeof timeline.find(".timeline-body").offset() !=undefined) {
			timeline.find(".timeline-body").scrollTop(currentElement.offset().top - timeline.find(".timeline-body").offset().top + timeline.find(".timeline-body").scrollTop());
		}
	}


	// Deacrivate active element on click
	function deactivate(){
		// Clear children timelines
		removeBackground()
		clearDesendants();
		// Remove background
		removeBackground();
		currentElement = null;
		currentID = null;
		timelineManager.config[objectType]["activeElementID"] = null;

		if (objectType != "comment"){
			// Clear description, media, and comments
			loadDescription();
			loadMedia();
			window.timelines.renderMiddleView('')
			if (commentTimeline())	
				commentTimeline().clearTimeline();
		}; 

	};

	// Timeline with comments
	function commentTimeline(){
		return timelineManager.config["comment"]["timeline"];
	};

	// Remove all items from this timeline
	this.clearTimeline = function(){
		timeline.find(".timeline-block").remove();
		hideNewElementButton();
		adjustTimelineHeight();
		clearDesendants();		
	}

	// Public method for clearDesendants()
	this.clearDesendants = function(){
		clearDesendants();
	}
	// Recursive function to clear all child timelines
	function clearDesendants(){
		var child;
		if (child = nestedTimeline()){
			child.clearTimeline();
			child.clearDesendants();
		};	
	};

	// Hide new element button
	function hideNewElementButton(){
		var button;
		if (button = newElementButton()){
			button.attr("href", "#")
			button.hide();				
		};		
	}

	// Load description, media, and comments
	function loadResources(){
			loadDescription();
			loadMedia();
			loadItems("comment");			
	
	};

	// Public method for loadDescendants()
	this.loadDescendants = function(){
		loadDescendants();
	};
	// Recursive function to fill all child timelines
	function loadDescendants(){
		var child;
		if (child = nestedTimeline()){
			child.loadCollection();
			child.loadDescendants();
		};	
	}

	// Whether timeline has nested timeline
	this.hasChild = function(){
		return timelineManager.config[objectType]["child"] ? true : false;
	}

	// Nested timeline
	function nestedTimeline(){
		var child;
		if (child = timelineManager.config[objectType]["child"]){
			return timelineManager.config[child]["timeline"]
		} else {
			return null;
		};
	};

	this.parentTimeline = function(){
		parentTimeline();
	};

	// Public timeline
	function parentTimeline(){
		var parent;
		if (parent = timelineManager.config[objectType]["parent"]){
			return timelineManager.config[parent]["timeline"]
		} else {
			return null;
		};
	};

	// Save clicks to database
	function trackClick(){
		$.ajax({ 
			dataType: "json",
			method: "post", 
			url:  "/clicks",
			data: {
				"click": {
					"clickable_type": timelineManager.capitalize(objectType),
					"clickable_id": currentID
				}
			}
		});
	};

	// Populate child timeline and add new item button
	function loadChildren(){
		var child;
		if (child = timelineManager.config[objectType]["child"]){
			loadItems(child);
			var button = nestedTimeline().newElementButton();
			button.attr("href", nestedURL(child) + "/new")
			button.show();
		};		
	};

	// Public method fornewElementButton()
	this.newElementButton = function(){
		return newElementButton();
	}
	// Button to add new element
	function newElementButton(){
		return timeline.find("header .new-timeline-item");
	}

	// Public method for loadCollection()
	this.loadCollection = function(){
		return loadCollection();
	};
	// Load elements for this timeline
	function loadCollection(){
		var data = defaultParams();
		data["page"] = 1;
		$.ajax({ dataType: "script", url: timelineManager.pluralize(objectType), data: data});
	};

	function loadItems(object){
		var data = object != "comment" ? defaultParams() : {};
		data["page"] = 1;
		$.ajax({ dataType: "script", url:  nestedURL(object), data: data});
	}

	// Remove background
	function removeBackground(){
		currentElement.parents(".timeline-body").find(".timeline-block").removeClass("active");
		currentElement.parents(".timeline-body").find(".timeline-block").removeClass("exactive");
	};

	// Add background
	function addBackground(){
		removeBackground();

		$(".active").addClass("exactive");
		currentElement.addClass("active");
	};

	// Load media files for active element
	this.loadMedia = function(){
		loadMedia();
	}
	function loadMedia(){
		var url = resourceURL(objectType, currentID, "resources")
		loadResource(url);
	};

	function loadResource(url, callback){
		$.ajax({ dataType: "script", url: url, success: callback});
	};

	// Load description for active element
	function loadDescription(){
		var url = resourceURL(objectType, currentID, "descriptions")
		console.log(url)
		loadResource(url);
	};

}
