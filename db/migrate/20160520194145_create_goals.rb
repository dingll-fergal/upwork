class CreateGoals < ActiveRecord::Migration
  def change
    create_table :goals do |t|
      t.references :player, index: true
      t.references :match, index: true
      t.string :minute
      t.boolean :offset
      t.boolean :penalty, default: false
      t.boolean :ownapal, default: false

      t.timestamps null: false
    end
  end
end
