class Level2 < ActiveRecord::Base
  # Level2(id: integer, name: string, visible: boolean, user_id: integer, date: datetime, avatar_file_name: string, avatar_content_type: string, avatar_file_size: integer, avatar_updated_at: datetime, description: text, created_at: datetime, updated_at: datetime)

  extend FriendlyId
  friendly_id :name, use: [:history, :finders]
  include Resourceable

  scope :visible, -> { where(visible: true) }

  belongs_to :user
  has_many :comments, as: :commentable, dependent: :destroy
  has_many :clicks, as: :clickable, dependent: :destroy
  
  has_many :descriptions, as: :descriptable, dependent: :destroy
  has_many :favourites, as: :favouritable, dependent: :destroy

  has_and_belongs_to_many :level1s
  has_many :level3s, dependent: :destroy

  has_attached_file :avatar
  validates_attachment_content_type :avatar, :content_type => [/\Aimage\/.*\Z/]


  validates_presence_of :name
  validates_length_of :name, :maximum => 40
  # validates :description, uniqueness: true, :allow_blank => true

  has_one :news_article, dependent: :destroy
  has_one :match, dependent: :destroy

  attr_accessor :level1_id

  def self.child_name
    "level3"
  end

  attr_accessor :distance_in_time

  self.per_page = 20

  after_save :add_description

  # <overview>  Add :distance_in_time to objects from collection
  # <return> ActiveRecord Collection
  scope :calculate_distance_in_time, lambda { |granularity=nil|
    time = nil
    all.each do |object|

      this_time = object.date
      object.distance_in_time = time ? time_distance(time, this_time, granularity) : 0.01


      time = this_time
    end
  }

  # <overview> Distance between two dates
  # <param> [Datetime] First date
  # <param> [Datetime] Second date
  # <param> [String] Granularity
  # <return>	[Integer]
  def self.time_distance(a, b, granularity)
    return 0 unless granularity

    case granularity
      when "minute"
        distance = (a.day - b.day) * 60 * 24 + (a.hour - b.hour) * 60 * (a.minute - b.minute)
      when "day"
        distance = (a.year - b.year) * 365 + (a.month - b.month) * 30 + (a.day - b.day)
      when "month"
        distance = (a.year - b.year) * 12 + (a.month - b.month)
      when "year"
        distance = a.year - b.year
      when "decade"
        distance = (a.year - b.year) / 10
    end
  end

  # <overview> Helper method for timelines
  # <return> Array
  def parentID
    level1s.pluck(:id)
  end

  # <overview> All objects whose name contains query
  # <param> [String] query
  # <return>	ActiveRecord Collection
  def self.search query = nil
    result = all
    result = where("LOWER(name) LIKE :q", q: "%#{query.downcase}%") if query
    return result
  end

  # <overview> Update history of description changes
  def add_description
    if descriptions.any?
      if descriptions.last.content != description
        descriptions.create(content: description)
      end
    else
      descriptions.create(content: description)
    end
  end


  # should change this method
  def child_resources
    Resource.joins("inner join level3s on level3s.id = resources.mediable_id AND resources.mediable_type = 'Level3'").where("level3s.level2_id = ?", id )
  end

  #show on expansive view top fotos based on clicks
  def load_level3s_photos
    level3_ids = level3s.pluck(:id)
    return [] if level3_ids.blank?
    top_fifty_resurces = Resource.select("resources.*, count(clicks.id) as click_counts")
            .joins( "INNER JOIN clicks ON clicks.clickable_id = resources.id AND clicks.clickable_type='Resource'" )
            .where("(resources.mediable_id IN (?) AND resources.mediable_type='Level3')", level3_ids)
            .group("resources.id")
            .order("click_counts DESC").limit(50)
    if (!top_fifty_resurces.blank? && !(level3_ids - top_fifty_resurces.map(&:mediable_id).uniq).blank?)
      other_resources = Resource.select("resources.*")
            .where("(resources.mediable_id IN (?) AND resources.mediable_type='Level3')", level3_ids - top_fifty_resurces.map(&:mediable_id).uniq)
            .group("resources.mediable_id")
      top_fifty_resurces = top_fifty_resurces + other_resources if other_resources
    elsif top_fifty_resurces.blank?
      top_fifty_resurces = Resource.select("resources.*")
            .where("(resources.mediable_id IN (?) AND resources.mediable_type='Level3')", level3_ids)
            .group("resources.mediable_id")     
    end
    top_fifty_resurces
  end

end
