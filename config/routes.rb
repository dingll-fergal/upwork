Rails.application.routes.draw do
  mount Ckeditor::Engine => '/ckeditor'
	devise_for :users, controllers: {
			omniauth_callbacks: "users/omniauth_callbacks"
	}

	root 'pages#index'

	get 'search' => 'pages#main'
	get 'about' => 'pages#about'
	get 'admin' => 'pages#admin'
	get 'settings' => 'pages#settings'
	get 'compare' => 'compare#compare'

	get "mailbox/inbox" => "mailbox#inbox", as: :mailbox_inbox
  get "mailbox/sent" => "mailbox#sent", as: :mailbox_sent
  get "mailbox/trash" => "mailbox#trash", as: :mailbox_trash

  resources :conversations do
    member do
      post :reply
      post :trash
      post :untrash
    end
    collection do
    	get :get_new_messages
    end
  end

	resources :settings, only: [:create, :update]

	resources :level1s do
		resources :comments, only: [:index, :new]
		resources :descriptions, only: :index
		resources :resources, only: [:index, :new]
  		resources :level2s do
			patch :remove, on: :member 
  			resources :level3s
  		end
    collection do
      get :filter_records
    end
    get :get_details, on: :member
	end

	resources :level2s, except: [:new, :create] do
		resources :comments, only: [:index, :new]
		resources :descriptions, only: :index
		resources :resources, only: [:index, :new]
		patch :add_level1, on: :member
  	resources :level3s
    get :get_details, on: :member
  end

	resources :level3s, except: [:new, :create] do
		resources :comments, only: [:index, :new]
		resources :resources, only: [:index, :new]
		resources :descriptions, only: :index
    get :get_details, on: :member
	end
	
	resources :comments

  resources :descriptions
  
  resources :resources do
    get :show_resource, on: :member
  	resources :comments, only: [:index, :new] 
  	resources :descriptions, only: :index
  end

	resources :users do
		resources :favourites
	end

	resources :clicks
	resources :favourites

end
