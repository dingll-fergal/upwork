class Level1sController < ApplicationController
	before_filter :authenticate_user!
	before_filter :fetch_level1, only: [:show, :edit, :update, :destroy]
	before_filter :authenticate_owner, only: [:edit, :update, :destroy]


	# GET /level1s.(js|html|json)
	def index
		# Retrieve all visible level1s whose name contain params[:query] ordered by params[:order] or date
		@level1s = Level1.search(params[:query]).order(params[:order] || "name asc")

		# Filter elements by ID
		if params[:filter]
			@level1s = @level1s.where(filters)
		end

		# Pagination
		if params[:page]
			@level1s = @level1s.visible.paginate(:page => params[:page])
		end

		# Add :distance_in_time field to all elements of @level1s collection
		if params[:granularity]
			@level1s = @level1s.calculate_distance_in_time(params[:granularity])
		end

		respond_to do |format|
        	format.html
        	format.js
        	format.json { render json: @level1s, status: 200 }
    	end
	end

	def filter_records
		@level1s = Level1.get_maching_records(params[:query], params[:search])
		respond_to do |format|
    	format.html
    	format.js
    	format.json { render json: @level1s, status: 200 }
		end
	end

	# GET /level1s/:id.(html|json)
	def show
		respond_to do |format|
        	format.html
        	format.js
        	format.json { render json: @level1, status: 200, location: @level1}
    	end
	end

	# GET /level1s/new.(html|js)
	def new
		@level1 = Level1.new
		respond_to do |format|
        	format.html
        	format.js
    	end
	end

	# POST /level1s.(html|js)
	def create
		@level1 = current_user.level1s.new(level1_params)

		if @level1.save
			respond_to do |format|
	        	format.html { redirect_to @level1, notice: 'Level1 was successfully created.'}
	        	format.json { render json: @level1, status: 200}
	        	format.js
	    	end
		else
			raise @level1.errors.to_json
			respond_to do |format|
				format.json { render status: 422}
	        	format.html { render :new}
	        	format.js
	    	end	
		end
	end

	# GET /level1s/:id/:edit.(html|js)
	def edit
		respond_to do |format|
        	format.html
        	format.js
    	end
	end

	# PUT /level1s/:id.(html|js|json)
	def update	
		if @level1.update(level1_params)
			respond_to do |format|
	        	format.html { redirect_to @level1, notice: 'Level1 was successfully updated.'}
	        	format.json { render json: @level1, status: 200}
	        	format.js
	    	end
		else
			respond_to do |format|
	        	format.html { render :edit}
	        	format.js
	        	format.json { render json: @level1.errors, status: 422 }
	    	end			
		end
	end

	# DELETE /level1s/:id.(html|json)
	def destroy
	    if @level1.destroy
	    	respond_to do |format|
	        	format.html {redirect_to @level1, notice: 'Level1 was successfully destroyed.'}
	        	format.json { head 204}
	    	end
	    else
	    	respond_to do |format|
	    		format.html { redirect_to :back, alert: "Level1 wasn't destroyed"}
	    		format.json { render json: @level1.errors, status: :unprocessable_entity }
	    	end
	    end
	end

	private

	# block access unless user has privilegies or is level1's owner
	def authenticate_owner
		block_access unless (@level1.user == current_user) || current_user.admin? || current_user.super_user?
	end

	def level1_params
    	params.require(:level1).permit(:name, :visible, :date, :avatar, :description, :list_price, :discounted_price)
  	end 

  	def fetch_level1
  		@level1 = Level1.find(params[:id])
  	end

end
