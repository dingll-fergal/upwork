class Resource < ActiveRecord::Migration
  def change
  	create_table :resources do |t|
  		t.attachment :content
    	t.references :mediable, polymorphic: true, index: true
    	t.attachment :avatar
    	t.text :description
    	t.string :video_url

    	t.timestamps null: false
    end
  end
end
