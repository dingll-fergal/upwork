class CreateClicks < ActiveRecord::Migration
  def change
    create_table :clicks do |t|
    	t.references :user, null: false
    	t.references :clickable, polymorphic: true, index: true

    	t.timestamps null: false
    end
  end
end
